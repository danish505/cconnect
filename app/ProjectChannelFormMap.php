<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectChannelFormMap extends Model
{
    //
    protected $table = 'project_channel_form_map';


    protected $fillable = [
        'project_id', 'form_id','channel_id'
    ];
}
