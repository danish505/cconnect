<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City_project_map extends Model
{
    //
    protected $table = 'city_project_map';


    protected $fillable = [
        'project_id', 'city_id'
    ];
}
