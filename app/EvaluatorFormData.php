<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluatorFormData extends Model
{
    //
    protected $table = 'evaluators_form_data';
}
