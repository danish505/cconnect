<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    //
    protected $guard = 'client';

    protected $fillable = [
        'email', 'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];


}
