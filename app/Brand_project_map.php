<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand_project_map extends Model
{
    //
    protected $table = 'brand_project_map';



    protected $fillable = [
        'project_id', 'brand_id'
    ];
}
