<?php

namespace App\Http\Controllers;

use App\Breakins;
use App\Clock;
use App\EmployeeProjectMap;
use App\EvaluatorForm;
use App\EvaluatorFormData;
use App\Evaluators;
use App\Form_data;
use App\Projects;
use App\TrainerForm;
use App\TrainerFormData;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;


    class ApiController extends Controller
    {
        //

        public function __construct()
        {
            $this->middleware('auth:api', ['except' => ['login']]);
        }


        /*
         * Clock in and clock out  start
         * */

        public function clock_in(Request $request)
        {

            $date =  Carbon::now('Asia/Karachi');
            $clock_in = DB::table('clock')
               ->select('channels.name as channel_name','projects.name as project_name','clock.clock_in')
                ->leftJoin('channels','channels.id','=','clock.channel_id')
                ->leftJoin('projects','projects.id','=','clock.project_id')
                ->whereDate('clock_in',Carbon::today('Asia/Karachi'))
                ->where('user_id',$request->user_id)
                ->where('clock_out',null)
               ->orderBy('clock.id','desc')
               ->limit(1)
               ->get()->toArray();

            if($clock_in){
                $message = array(
    //                . $clock_in[0]->channel_name."and project".$clock_in[0]->project_name
                    'message'=>"you already clocked in",
                    'project'=> $clock_in[0]->project_name,
                    'channel'=> $clock_in[0]->channel_name,
                    'clock_in'=> $clock_in[0]->clock_in,
                    'status'=>'0'
                );
                return json_encode($message);

            }
            else{
                $clock = new Clock();

                $clock->user_id = $request->user_id;
                $clock->channel_id = $request->channel_id;
                $clock->project_id = $request->project_id;
                $clock->clock_in =  Carbon::now('Asia/Karachi');
                if($request->hasFile('photo')){

                    $file = $request->file('photo');
                    $extension = $file->getClientOriginalExtension();
                    $filename = time().'.'.$extension;
                    $file->move('uploads/clock_in',$filename);
                }
                $clock->clockin_image = $filename;
                $clock->clockin_lat = $request->lat;
                $clock->clockin_log = $request->log;
                $clock->created_ip = $request->ip();

                $clock->save();

                $message = array(
                    'message'=>'Clock in successfully',
                    'Time'=> $date->isoFormat('h:mm:ss a'),
                    'Date'=> $date->isoFormat('Do MM YYYY'),
                    'status'=>'1'
                );
                return json_encode($message);
            }

        }


        public function check_clockin(Request $request)
        {
            $date =  Carbon::today('Asia/Karachi');
            $clock_in = DB::table('clock')
                ->leftJoin('channels','channels.id','=','clock.channel_id')
                ->whereDate('clock_in',$date)
                ->where('user_id',$request->user_id)
                ->where('clock_out',null)
                ->whereDate('clock_in',Carbon::today('Asia/Karachi'))
                ->orderBy('clock.id','desc')
                ->limit(1)
                ->get()->toArray();


            if($clock_in){
                $message = array(
                    'message'=>'You are already clocked in.',
                    'channel'=>$clock_in[0]->name,
                    'status'=>'0'
                );
                return json_encode($message);

            }

            $message = array(
                'message'=>'You can clock in.',
                'status'=>'1'
            );
            return json_encode($message);

        }

        public function check_clockout(Request $request)
        {
            $date = Carbon::today('Asia/Karachi');
            $clock_in = DB::table('clock')->whereDate('clock_in', $date)
                ->where('user_id', $request->user_id)
                ->where('clock_out', null)
                ->orderBy('clock.id','desc')
                ->limit(1)
                ->get()->toArray();


            if ($clock_in) {
                $break_out = DB::table('break')
                    ->whereDate('break_in', Carbon::today('Asia/Karachi'))
                    ->where('user_id', $request->user_id)
                    ->where('break_out', null)
                    ->orderBy('break.id','desc')
                    ->limit(1)
                    ->get()->toArray();


                if ($break_out) {
                    $error_message = array(
                        'message' => 'You have not breaked out.',
                        'status' => '0'
                    );
                    return json_encode($error_message);
                }
                $message = array(

                    'message'=>'You can clock out.',
                    'status'=>'1'
                );

                return json_encode($message);


            }
            $message = array(
                'message'=>'You need to clock in first.',
                'status'=>'0'
            );
            return json_encode($message);


        }



        public function clock_out(Request $request)
      {
                /*
                * clocking out user if he has has clocked in
                */

                $date =  Carbon::now('Asia/Karachi');

//                $date = Carbon::now();

                $clock_in = DB::table('clock')
                ->whereDate('clock_in','>=',$date->subHour(12))
                ->where('user_id',$request->user_id)
                ->where('clock_out',null)
                ->orderBy('id','desc')
                ->limit(1)
                ->get()->toArray();


             if($clock_in){
             $break_out = DB::table('break')
                 ->whereDate('break_in',Carbon::today())
                 ->where('user_id',$request->user_id)
                 ->where('break_out',null)
                 ->orderBy('id','desc')
                 ->limit(1)
                 ->get()->toArray();


             if($break_out){
                 $error_message = array(
                     'message'=>'You have not breaked out.',
                     'break_in'=>$break_out[0]->break_in,
                     'status'=>'0'
                 );
                 return json_encode($error_message);
             }
                 if($request->hasFile('photo')){

                     $file = $request->file('photo');
                     $extension = $file->getClientOriginalExtension();
                     $filename = time().'.'.$extension;
                     $file->move('uploads/clock_out',$filename);
                 }

                 $clock = DB::table('clock')
                     ->where('user_id', $request->user_id)
                     ->whereDate('clock_in','>=',$date->subHours(12))
                     ->limit(1)
                     ->orderBy('clock.id','desc')
                     ->update(['clock_out' => Carbon::now('Asia/Karachi'),
                         'clockout_image'=>$filename,'updated_ip'=>$request->ip(),
                         'clockout_lat'=>$request->lat,'clockout_log'=>$request->log
                     ]);

    //


              if($clock){

                $message = array(
                    'message'=>'Clock out successfully',
                    'Time'=> $date->isoFormat('h:mm:ss a'),
                    'Date'=> $date->isoFormat('Do MM YYYY'),
                    'status'=>'1'
                );
                return json_encode($message);
            }

            }

              else{
                  $error_message = array(
                      'message'=>'You are not clocked in.',
                      'status'=>'0'
                  );
                  return json_encode($error_message);
              }

            }

        /*
         * Clock in and clock out  End
         *
         * */


        /*
         * CHECK BREAK IN AND BREAK OUT START
         *
         * */

        public function check_breakin(Request $request)
        {

            $date =  Carbon::now('Asia/Karachi');

            $clock_in = DB::table('clock')
                ->whereDate('clock_in',Carbon::today())
                ->where('user_id',$request->user_id)
                ->where('clock_out',null)
                ->orderBy('id','desc')
                ->limit(1)
                ->get()->toArray();


            if($clock_in) {

                $break_in = DB::table('break')->whereDate('break_in',Carbon::today())
                    ->leftJoin('channels','channels.id','=','break.channel_id')
                    ->where('user_id',$request->user_id)
                    ->where('break_out',null)
                    ->whereDate('break_in',Carbon::today())
                    ->orderBy('break.id','desc')
                    ->limit(1)
                    ->get()->toArray();

                if ($break_in) {
                    $message = array(
                        'message' => 'You are already breaked in.',
                        'channel' => $break_in[0]->name,
                        'status' => '0'
                    );
                    return json_encode($message);

                }

                $message = array(
                    'message' => 'You can break in.',
                    'status' => '1'
                );
                return json_encode($message);
            }

            $message = array(
            'message' => 'You need to clockin first.',
            'status' => '0'
        );
            return json_encode($message);


        }



        public function check_breakout(Request $request)
        {
            $clock_in = DB::table('clock')->whereDate('clock_in', Carbon::today())
                ->where('user_id', $request->user_id)
                ->where('clock_out', null)
                ->orderBy('clock.id','desc')
                ->limit(1)
                ->get()->toArray();


            if ($clock_in) {
                $break_out = DB::table('break')
                    ->whereDate('break_in', Carbon::today())
                    ->where('user_id', $request->user_id)
                    ->where('break_out', null)
                    ->orderBy('break.id','desc')
                    ->limit(1)
                    ->get()->toArray();


                if ($break_out) {
                      $error_message = array(
                        'message' => 'You can break out.',
                        'status' => '1'
                    );
                    return json_encode($error_message);
                }
                $message = array(

                    'message'=>'You are not in a break',
                    'status'=>'0'
                );

                return json_encode($message);


            }
            $message = array(
                'message'=>'You need to clock in first.',
                'status'=>'0'
            );
            return json_encode($message);


        }

        /*
            * CHECK_BREAK IN AND BREAK OUT END
            *
            * */


        /*
        * Break in and Break out Start
        *
        * */

        public function break_in(Request $request)
        {
            $date =  Carbon::now('Asia/Karachi');

            $clock_in = DB::table('clock')
                ->where('user_id',$request->user_id)
                ->whereDate('clock_in',Carbon::today())
                ->where('clock_out',null)
                ->orderBy('id','desc')
                ->limit(1)
                ->get()->toArray();



            $clock_out = DB::table('clock')
                ->whereDate('clock_out',Carbon::today())
                ->where('user_id',$request->user_id)
                ->where('clock_out',true)
                ->orderBy('id','desc')
                ->limit(1)
                ->get()->toArray();


            if($clock_out){

                $message = array(
                    'message'=>'You clocked out today.',
                    'clockout'=>$clock_out[0]->clock_out,
                    'status'=>'0'
                );
                return json_encode($message);
            }


            if($clock_in){
                $break_in = DB::table('break')
                    ->select('projects.name as project_name','channels.name as channel_name','break.break_in')
                    ->leftJoin('projects','projects.id','=','break.project_id')
                    ->leftJoin('channels','channels.id','=','break.channel_id')
                    ->where('user_id',$request->user_id)
                    ->whereDate('break_in',Carbon::today())
                    ->where('break_out',null)
                    ->orderBy('break.id','desc')
                    ->limit(1)
                    ->get()->toArray();



                if($break_in) {
                    $message = array(
                        'message'=>'You are already on a break.',
                        'project'=>$break_in[0]->project_name,
                        'channel'=>$break_in[0]->channel_name,
                        'breakin'=>$break_in[0]->break_in,
                        'status'=>'0'
                    );
                    return json_encode($message);
                }



                $break = new Breakins();
                if($request->hasFile('photo')){

                    $file = $request->file('photo');
                    $extension = $file->getClientOriginalExtension();
                    $filename = time().'.'.$extension;
                    $file->move('uploads/break_in',$filename);
                }
                foreach($clock_in as $run):

                    $break->project_id = $run->project_id;
                    $break->user_id = $run->user_id;
                    $break->channel_id= $run->channel_id;
                    $break->breakin_image = $filename;
                    $break->break_in =  Carbon::now('Asia/Karachi');
                    $break->created_ip = $request->ip();
                    $break->lat = $request->lat;
                    $break->log = $request->log;
                    $break->save();
                endforeach;

                $message = array(
                    'message'=>'Break in successfully.',
                    'Time'=> $date->isoFormat('h:mm:ss a'),
                    'Date'=> $date->isoFormat('Do MM YYYY'),
                    'status'=>'1'
                );
                return json_encode($message);

            }
            else{
                $message = array(
                    'message'=>'You are not clocked in today.',
                    'status'=>'0'
                );
                return json_encode($message);
            }

        }

        public function break_out(Request $request)
        {
            $date =  Carbon::now('Asia/Karachi');
            $break_in = DB::table('break')->whereDate('break_in',Carbon::today())
                ->where('user_id',$request->user_id)
                ->whereDate('break_in',Carbon::today())
                ->where('break_out',null)
                ->limit(1)
                ->get()->toArray();

            $clock_out = DB::table('clock')
                ->whereDate('clock_in',Carbon::today())
                ->where('user_id',$request->user_id)
                ->where('clock_out',true)
                ->orderBy('id','desc')
                ->limit(1)
                ->get()->toArray();

            if($clock_out){
                $message = array(
                    'message' => 'You clocked out.',
                    'clock_out'=> $clock_out[0]->clock_out,
                    'status' => '1'
                );
                return json_encode($message);
            }


            if($break_in){

                if($request->hasFile('photo')){

                    $file = $request->file('photo');
                    $extension = $file->getClientOriginalExtension();
                    $filename = time().'.'.$extension;
                    $file->move('uploads/break_in',$filename);
                }

                $break = DB::table('break')
                ->where('user_id', $request->user_id)
                ->whereDate('break_in',$date)
                    ->orderBy('id','desc')
                ->update(['break_out' => Carbon::now('Asia/Karachi'),
                    'updated_ip'=>$request->ip(),'breakout_lat'=>$request->lat,
                    'breakout_log'=>$request->log,'breakout_image'=>$filename]);


                if($break){
                    $message = array(
                        'message' => 'Break out successfully.',
                        'Time'=> $date->isoFormat('h:mm:ss a'),
                        'Date'=> $date->isoFormat('Do MM YYYY'),
                        'status' => '1'
                    );
                    return json_encode($message);
                }

            }


                $error_message = array(
                    'message'=>'You are not in a break.',
                    'status'=>0
                );
                return json_encode($error_message);


        }


        public function form_show(Request $request)
        {
            $form = DB::table('project_channel_form_map')
                ->select('project_channel_form_map.project_id', 'project_channel_form_map.channel_id',
                    'project_channel_form_map.form_id','form.title', 'form.form_fields')
                ->leftJoin('form', 'form.id', '=', 'project_channel_form_map.form_id')
                ->leftJoin('channels', 'channels.id', '=', 'project_channel_form_map.channel_id')
                ->leftJoin('projects', 'projects.id', '=', 'project_channel_form_map.project_id')
                ->where('project_channel_form_map.channel_id', $request->channel_id)
                ->where('project_channel_form_map.project_id', $request->project_id)
                ->first();

            $form_channel = DB::table('channels')
                ->select('form.form_fields', 'form.title')
                ->leftJoin('form', 'form.id', '=', 'channels.form_id')
                ->where('channels.id', $request->channel_id)
                ->first();

            if ($form) {

                    $message = array(
                        'title' => $form->title,
                        'form_fields' => json_decode($form->form_fields),
                    );

                    return $message;
            }

                elseif ($form_channel) {
                     $message = array(
                        'title' => $form_channel->title,
                        'form_fields' => json_decode($form_channel->form_fields),
                    );

                    return $message;
            }

        }
    /*
     * Receiving Form Data
     *
     * */
        public function form_data_receive(Request $request)
        {
            $form =  DB::table('project_channel_form_map')
                ->select('project_channel_form_map.form_id')
                ->where('project_channel_form_map.channel_id',$request->channel_id)
                ->where('project_channel_form_map.project_id',$request->project_id)
                ->limit(1)
                ->get();

            if(sizeof($form) == 0){

                $message = array(
                    'message'=>'Form not found',
                    'status'=>0);
                return json_encode($message);

            }

                $data =  $form[0]->form_id;

                $form_data = new Form_data();

                $form_data->data = strip_tags($request->form_data);
                $form_data->form_id = $data;
                $form_data->user_id = $request->user_id;
                $form_data->created_ip = $request->ip();
                $form_data->created_at = Carbon::now('Asia/Karachi');
                $form_data->area = $request->area;
                $form_data->nearest_landmark =  $request->nearest_landmark;
                $form_data->area_code = $request->area_code;
                $form_data->project_id = $request->project_id;
                $form_data->channel_id =$request->channel_id;
                $form_data->city_id = $request->city_id;
                $form_data->lat = $request->lat;
                $form_data->log = $request->log;
                $form_data->save();


            $message = array(
                'message'=>'data save successfully',
                'status'=>1);
            return json_encode($message);


        }

        public function store_emp_project_map(Request $request)
        {
            $emp = DB::table('projects')
                ->where('id',$request->project_id)->get();

            $project = new EmployeeProjectMap();

            $project->employee_id = $request->employee_id;
            $project->project_id = $request->project_id;
            $project->channel_id = $request->channel_id;
            $project->city_id = $request->city_id;
            $project->brand_id = $request->brand_id;
            $project->team_id = $request->team_id;
            $project->agency_id = $emp[0]->agency_id;
            $project->status =0;

            $message = array(
                'message'=>'Employee Data inserted successfully',
                'status'=>1
            );

            if($project->save()){
                return json_encode($message);
            }

        }


        public function evaluator_form()
        {

            $evaluator_form = EvaluatorForm::orderBy('id','desc')->first();

            $arr_evalutor = array(
                'id'=>$evaluator_form->id,
                'title'=> $evaluator_form->title,
                'form_fields'=> json_decode($evaluator_form->form_fields),
            );

           return response()->json($arr_evalutor);

        }

        public function trainer_form()
        {

            $evaluator_form = TrainerForm::orderBy('id','desc')->first();

            $arr_trainer = array(
                'id'=>$evaluator_form->id,
                'title'=> $evaluator_form->title,
                'form_fields'=> json_decode($evaluator_form->form_fields),
            );

            return response()->json($arr_trainer);

        }
        public function trainer_form_get_data(Request $request)
        {

            $trainer_data = new TrainerFormData();

            $trainer_data->trainer_id = $request->user_id;
            $trainer_data->project_id = $request->project_id;
            $trainer_data->channel_id = $request->channel_id;
            $trainer_data->trainer_form_id = $request->form_id;
            $trainer_data->form_data = $request->form_data;
            $trainer_data->lat = $request->lat;
            $trainer_data->log = $request->log;

            if($trainer_data->save()){
                return response()->json("Data Save Successfully");
            }
            return response()->json('error occurred');

        }

        public function evaluator_form_get_data(Request $request)
        {

            $evaluator_data = new EvaluatorFormData();

            $evaluator_data->evaluator_id = $request->user_id;
            $evaluator_data->project_id = $request->project_id;
            $evaluator_data->channel_id = $request->channel_id;
            $evaluator_data->evaluator_form_id = $request->form_id;
            $evaluator_data->form_data = $request->form_data;

            if($evaluator_data->save()){
            return response()->json("Data Save Successfully");
            }
            return response()->json('error occurred');

        }


    }
