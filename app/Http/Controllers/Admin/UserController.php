<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use App\Brands;
use App\Channel_project_map;
use App\Channels;
use App\Cities;
use App\Employee;
use App\EmployeeProjectMap;
use App\Projects;
use App\Team;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        //
        $users = DB::table('employees')
        ->select('employees.id','channels.id as channel_id','channels.name as channel_name','employees.cnic',
        'employees.name as employee_name','employees.email','employees.status','projects.name as project_name',
            'agencies.name as agency_name','employees.image')
        ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
        ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
        ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
        ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
        ->where('employees.status','approved')
        ->paginate(15);

        
            $data['projects'] = Projects::all();
            $data['agencies'] = Agency::all();
            $data['employees'] = Employee::all();
            $data['channels']= Channels::all();

        return view('admin.users.manage_users',compact('users','data'));
    }


    public function user_profile($id)
    {

        $employee = DB::table('employees')
            ->select('employees.id as emp_id','employees.name','employees.email',
                'employees.cnic','employees.cell','employees.agency_email','employees.image',
                'projects.name as project_name','agencies.name as agency_name','cities.name as city_name',
                'channels.name as channel_name','teams.name as team_name','employees.image','employees.fathername','employees.cell',
            'employees.created_at')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('teams','teams.id','=','employee_project_map.team_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->leftJoin('users','users.id','=','employee_project_map.employee_id')
            ->where('employees.id',$id)
            ->first();

        $last_login = DB::table('employees')
            ->select('lat','log','user_id','login_session.created_at')
            ->leftJoin('login_session','login_session.user_id','=','employees.id')
            ->where('login_session.user_id',$employee->emp_id)
            ->orderBy('login_session.created_at')
            ->first();

        return view('admin.users.user_profile',compact('employee','last_login'));

    }


    /**
      USERS ON BREAK
     */
    public function users_break()
    {
        //

        $users = DB::table('break')
            ->select('break.user_id','employees.name','break.break_in',
                'break.break_out','break.lat','break.log','break.created_at',
                'break.updated_at','agencies.name as agency_name','cities.name as city_name',
                'projects.name as project_name','channels.name as channel_name',
                'employee_project_map.channel_id','break.breakin_image','break.breakout_lat','break.breakout_log')
            ->leftJoin('employees','employees.id','=','break.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->whereDate('break.break_in',Carbon::today())
            ->whereNull('break.break_out')
            ->orderBy('break.id','desc')
            ->paginate(15);

        $data['channels']= Channels::all();
        $data['projects'] = Projects::all();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();

        return view('admin.users.users_breakin',compact('users','data'));

    }


    public function users_break_search(Request $request)
    {
        //

        $user_search = DB::table('break')
            ->select('break.user_id','employees.name','break.break_in',
                'break.break_out','break.lat','break.log','break.created_at',
                'break.updated_at','agencies.name as agency_name','cities.name as city_name',
                'projects.name as project_name','channels.name as channel_name',
                'break.channel_id','break.breakin_image',
                'break.breakout_lat','break.breakout_log')
            ->leftJoin('employees','employees.id','=','break.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','break.user_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->whereDate('break.break_in',Carbon::today())
            ->whereNull('break.break_out')
            ->orderBy('break.id','desc');

        if($request->agency){
           $user_search->where('employee_project_map.agency_id',$request->agency);
        }
        if($request->channel){
            $user_search->where('break.channel_id',$request->channel);
        }
        if($request->project){
            $user_search->where('break.project_id',$request->project);
        }
        if($request->employee){
            $user_search->where('break.user_id',$request->employee);
        }
        $users= $user_search->paginate(15);

        $data['channels']= Channels::all();
        $data['projects'] = Projects::all();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
//
         return view('admin.users.users_breakin',compact('users','data'));
    }


    public function online_users()
    {
        $employees = DB::table('clock')
            ->select('employees.id as employee_id','employees.name as employee_name','employees.email','clock.clock_in',
                'employees.name','brands.id as brand_id','brands.name as brand_name',
                'cities.name as city_name','channels.name as channel_name','agencies.name as agency_name',
                'projects.name as project_name','projects.expiry_date','projects.id as project_id',
                'clock.channel_id','clock.clock_out','clock.clockin_image','clock.clockout_image',
                'clock.clockin_lat','clock.clockin_log')
            ->leftJoin('employees','employees.id','=','clock.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','clock.project_id')
            ->leftJoin('channels','channels.id','=','clock.channel_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
//            ->leftJoin('break','break.user_id','=','employees.id')
            ->whereDate('clock.clock_in',Carbon::today())
            ->whereNull('clock_out')
            ->whereDate('projects.expiry_date','>=',Carbon::today())
            ->orderBy('clock.created_at')
            ->orderBy('employee_project_map.created_at')
            ->paginate(15);
            
        
             
         
        
         
        
        $data['channels']= Channels::all();
        $data['projects'] = Projects::all();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
        $data['city'] = Cities::all();

        return view('admin.users.online_users_report',compact('employees','data'));
    }

    public function online_users_search(Request $request)
    {
        $emp = DB::table('clock')
           ->select('employees.id as employee_id','employees.name as employee_name','employees.email','clock.clock_in',
                'employees.name','brands.id as brand_id','brands.name as brand_name',
                'cities.name as city_name','channels.name as channel_name','agencies.name as agency_name',
                'projects.name as project_name','projects.expiry_date','projects.id as project_id',
                'clock.channel_id','clock.clock_out','clock.clockin_image','clock.clockout_image',
                'clock.clockin_lat','clock.clockin_log')
            ->leftJoin('employees','employees.id','=','clock.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','clock.project_id')
            ->leftJoin('channels','channels.id','=','clock.channel_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('break','break.user_id','=','clock.user_id')
            ->whereDate('clock.clock_in',Carbon::today())
            ->whereNull('clock_out')
            ->orderBy('clock.id','desc');
            if($request->employee){
                $emp->where('clock.user_id',$request->employee);
            }
            if($request->channel){
                $emp->where('clock.channel_id',$request->channel);
            }
            if($request->project){
                $emp->where('clock.project_id',$request->project);
            }

            $employees = $emp->paginate(15);
            
        // $break_array = array();

        // foreach($employees as $pro):
        //     $last_login = Employee::select('break.break_in','break.break_out','employees.id as user_id','break.lat','break.log')
        //         ->leftJoin('break','break.user_id','=','employees.id')
        //         ->where('break.user_id',$pro->employee_id)
        //         ->whereDate('break.break_in',Carbon::today())
        //         ->orderBy('break.id','desc')
        //         ->first();
                
            
        //     $break_array[] = $last_login;
        //     endforeach;
         
            
            
        
        $data['channels']= Channels::all();
        $data['projects'] = Projects::all();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
        $data['city'] = Cities::all();

        return view('admin.users.online_users_report',compact('employees','data'));

    }

    public function manage_channels()
    {
       $users =  DB::table('employees')
            ->select('employees.id','employee_project_map.project_id','employee_project_map.channel_id'
                ,'employee_project_map.city_id','employee_project_map.brand_id','employees.name as employee_name'
                ,'employee_project_map.status','projects.name as project_name','employees.cnic','channels.name as channel_name'
                ,'employees.email','employees.status')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('teams','teams.id','=','employee_project_map.team_id')
            ->orderBy('employees.id','desc')
            ->get()->toArray();

        $channels = Channels::all()->toArray();

        return view('admin.users.manage_channels',compact('users','channels'));
    }

    public function status_update(Request $request, $id)
    {

        $update =  Employee::find($id);
        $update->status = $request->status;
        $update->save();
        return redirect('admin/');
    }
    /*
     * USER CHANNEL SEARCH
     *
     * */

    public function user_search_channels(Request $request)
    {
       $user_search =  DB::table('employees')
            ->select('employees.id','employee_project_map.project_id','employee_project_map.channel_id'
                ,'employee_project_map.city_id','employee_project_map.brand_id','employees.name as employee_name'
                ,'employee_project_map.status','projects.name as project_name','employees.cnic','channels.name as channel_name'
                ,'employees.email','employees.status')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id');

       if($request->name){
           $user_search->where('employees.name', $request->name );
       }
       if($request->project){
           $user_search->where('projects.name', $request->project );
       }
       if($request->channel){
           $user_search->where('channels.name', $request->channel);
       }

       if($request->start_date){
           $user_search->where('projects.start_date', $request->start_date);
       }
       if($request->expiry_date){
           $user_search->where('projects.expiry_date',  $request->expiry_date );
       }
       $users = $user_search->paginate(15);


        $channels = Channels::all()->toArray();
        return view('admin.users.manage_channels',compact('users','channels'));
    }


/*
 * USER SEARCH
 *
 * */

    public function user_search(Request $request)
    {

        $user_search = DB::table('employees')
            ->select('employees.id','channels.id as channel_id','channels.name as channel_name',
                'employees.cnic','employees.name as employee_name','employees.email',
                'employees.status','projects.name as project_name','agencies.name as agency_name')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id');
           if($request->project){
               $user_search->where('employee_project_map.project_id', $request->project );
           }
           if($request->cnic){
               $user_search->where('employees.cnic',  $request->cnic );
           }
           if($request->agency){
               $user_search->where('employee_project_map.agency_id', $request->agency );
           }
           if($request->channel){
               $user_search->where('employee_project_map.channel_id',  $request->channel );
           }
           $users = $user_search->paginate(15);

            $data['projects'] = Projects::where('is_deleted',0)->get();
            $data['agencies'] = Agency::all();
            $data['employees'] = Employee::all();
            $data['channels'] = Channels::all();
            
        return view('admin.users.manage_users',compact('users','data'));

    }

    /**
     * On Ground Projects
     *
     */


    public function register_user()
    {


//        $project_array = array();
        $login_array = array();
//
        $projects = DB::table('projects')
            ->select('projects.name as project_name','projects.expiry_date','projects.id')
            ->orderBy('projects.id', 'desc')
            ->get();
    

            $users = DB::table('employees')
                ->select('employee_project_map.project_id',
                    'cities.name as city_name', 'agencies.name as agency_name', 'brands.name as brand_name', 'employee_project_map.team_id', 'employee_project_map.channel_id', 'employees.name as username', 'employees.created_at', 'employees.name as emp_name', 'employees.id as employee_id', 'employee_project_map.agency_id', 'employee_project_map.brand_id', 'employees.cnic' )
                ->leftJoin('employee_project_map', 'employee_project_map.employee_id', '=', 'employees.id')
                ->leftJoin('cities', 'cities.id', '=', 'employee_project_map.city_id')
                ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id')
                ->leftJoin('brands', 'brands.id', '=', 'employee_project_map.brand_id')
                ->leftJoin('agencies', 'agencies.id', '=', 'employee_project_map.agency_id')
                ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
                ->whereDate('projects.expiry_date', '>=', Carbon::today())
                ->paginate(15);

            foreach ($users as $pro):

                $last_login = DB::table('login_session')
                    ->select('lat', 'log', 'user_id', 'created_at')
                    ->where('login_session.user_id', $pro->employee_id)
                    ->orderBy('login_session.id','desc')
                    ->first();

                $login_array[] = $last_login;

            endforeach;
           

      

            $data['agency'] = Agency::all();
            $data['project'] = Projects::where('is_deleted',0)->get();
            $data['cities'] = Cities::all();
            $data['employee'] = Employee::all();
            $data['channels'] = Channels::all();

            return view('admin.users.registered_users_report', compact('users', 'data', 'login_array','projects'));

        }





    public function register_user_search(Request $request)
    {
      $login_array = array();
$login_array = array();
//
        $projects = DB::table('projects')
            ->select('projects.name as project_name','projects.expiry_date','projects.id')
            ->orderBy('projects.id', 'desc')
            ->where('projects.expiry_date', '>', Carbon::today())
            ->get();


//        $emp_array = array();
        

            $reg_user = DB::table('employees')
                ->select('employee_project_map.project_id',
                    'cities.name as city_name', 'agencies.name as agency_name', 'brands.name as brand_name', 'employee_project_map.team_id', 'employee_project_map.channel_id', 'employees.name as username', 'employees.created_at', 'employees.name as emp_name', 'employees.id as employee_id', 'employee_project_map.agency_id', 'employee_project_map.brand_id', 'employees.cnic' )
                ->leftJoin('employee_project_map', 'employee_project_map.employee_id', '=', 'employees.id')
                ->leftJoin('cities', 'cities.id', '=', 'employee_project_map.city_id')
                ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id')
                ->leftJoin('brands', 'brands.id', '=', 'employee_project_map.brand_id')
                ->leftJoin('agencies', 'agencies.id', '=', 'employee_project_map.agency_id')
                ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
                ->whereDate('projects.expiry_date', '>=', Carbon::today());
                 
                 

            if($request->employee){
                $reg_user->where('employee_project_map.employee_id',$request->employee);
            }
            if($request->project){
                $reg_user->where('employee_project_map.project_id',$request->project);
            }
            if($request->city){
                $reg_user->where('employee_project_map.city_id',$request->city);
            }
            if($request->agency){
                $reg_user->where('employee_project_map.agency_id',$request->agency);
            }
            if($request->channel){
                $reg_user->where('employee_project_map.agency_id',$request->channel);
            }
            $users = $reg_user->paginate(15);
         
         foreach ($users as $pro):

                $last_login = DB::table('login_session')
                    ->select('lat', 'log', 'user_id', 'created_at')
                    ->where('login_session.user_id', $pro->employee_id)
                    ->orderBy('login_session.id','desc')
                    ->first();

                $login_array[] = $last_login;

            endforeach;
             


        $data['channels'] = Channels::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
        $data['cities'] = Cities::all();
        $data['employee'] = Employee::all();

        return view('admin.users.registered_users_report',compact('users','data','login_array','projects'));

    }


    public function store(Request $request)
    {
        //

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = DB::table('employee_project_map')
            ->select('employee_project_map.employee_id','employee_project_map.project_id',
                'employee_project_map.channel_id','employee_project_map.city_id','employee_project_map.brand_id',
                'employees.name as employee_name','employee_project_map.status','channels.name as channel_name')
            ->leftJoin('employees','employees.id','=','employee_project_map.employee_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->where('employee_project_map.employee_id',$id)
            ->get();

        $employees =  json_decode($employees, true);

        $channel = DB::table('employee_project_map')
            ->select('projects.channel_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->get();

        foreach($channel as $chann):

        $project_channel = DB::table('channels')
            ->whereIn('id',explode(',',$chann->channel_id))
            ->get();

        endforeach;


        return view('admin.users.edit_user_channels',compact('employees','project_channel'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
        //
        $employees = EmployeeProjectMap::where('employee_id',$id)->first();
        $employees->channel_id = implode(',',$request->channels);
        $employees->save();

        return redirect('admin/users/channels');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
}
