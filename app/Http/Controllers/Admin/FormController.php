<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use App\Channels;
use App\Cities;
use App\Employee;
use App\Form;
use App\Form_data;
use App\ProjectChannelFormMap;
use App\Projects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        //
        $forms = Form::where('is_public',1)->orderBy('id','desc')->get();
        return view('admin.forms.manage_forms',compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.forms.create_form');

    }

    /**
     * Store a newly created resource in storage it`s a callback function.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $channel = DB::table('channels')
            ->where('id',$request->id)
            ->get();
        $input = strip_tags($request->formStructure);


        $output = json_decode($input);

        $form = new Form();

        $form->user_id = auth()->user()->id;
        $form->title = $request->title;
        $form->form_fields = $output;
        $form->form_settings = 1;
        $form->is_public = 1;
        $form->status = 'approved';
        $form->created_ip = $request->ip();
        $form->save();

    }

    public function form_data_search(Request $request)
    {

        $data = DB::table('form_data')
            ->select('form_data.id','form_data.project_id','form_data.channel_id',
                'form_data.city_id','form.title','cities.name as city_name','channels.name as channel_name',
                'projects.name as project_name','projects.start_date','projects.expiry_date')
            ->leftJoin('projects','projects.id','=','form_data.project_id')
            ->leftJoin('channels','channels.id','=','form_data.channel_id')
            ->leftJoin('cities','cities.id','=','form_data.city_id')
            ->leftJoin('form','form.id','=','form_data.form_id');
        if($request->project ){
            $data->orWhere('projects.name',  $request->project );
        }
        if($request->channel){
        $data->where('channels.name',  $request->channel );
        }
        if($request->city){
            $data->where('cities.name', $request->city );
        }
        if($request->start_date)
        {
            $data->where('projects.start_date',$request->start_date );
        }
        if($request->expiry_date){
            $data->where('projects.expiry_date', $request->expiry_date );
        }
        $search = $data->paginate(15);



        return view('admin.forms.search_form_data',compact('search'));
//        $project = Projects::find();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $forms = Form::find($id);

        return view('admin.forms.view_forms',compact('forms'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $form = Form::find($id);

        return view('admin.forms.edit_forms',compact('form'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request, $id)
    {
        //
        $status = Form::find($id);
        $status->status = $request->status;;
        $status->updated_ip = $request->ip();
        $status->save();

        return redirect('admin/forms');

    }

    public function form_data_filters()
    {
        $data['project'] = Projects::where('is_deleted',0)->get();
        return view('admin.forms.form_data_filter',compact('data'));
    }


    public function update(Request $request,$id)
    {
       

        $input = strip_tags($request->formStructure);
        $output = json_decode($input);

        $form  = Form::find($id);
        $form->title = $request->title;
        $form->form_fields =  $output;
        $form->updated_ip = $request->ip();
        $form->save();


    }
    
    public function update_project_form(Request $request,$id)
    {
       
          $channel = DB::table('channels')
            ->where('id',$id)
            ->get();

    }



    public function store_project_form_map(Request $request)
    {

        $project_map = new ProjectChannelFormMap();

        $project_map->project_id = $request->project;
        $project_map->channel_id = $request->channel;
        $project_map->form_id = $request->form;
        $project_map->save();
        return redirect('admin/forms/project/map');

    }
    public function project_form_map_callback(Request $request)
    {

//        $projects = DB::table('projects')
//            ->select('projects.channel_id','projects.id','channels.name as channels_name')
//            ->leftJoin('channels','channels.id','=','projects.channel_id')
//            ->where('projects.id',$request->id)
//            ->get()->toArray();


//        $channels = Channels::whereIn('id',explode(',',$projects[0]->channel_id))->get()->toArray();

        $data['channel_map'] = DB::table('project_channel_form_map')
            ->select('channels.name','channels.id','project_channel_form_map.form_id')
            ->leftJoin('channels','channels.id','=','project_channel_form_map.channel_id')
            ->leftJoin('projects','projects.id','=','project_channel_form_map.project_id')
            ->where('project_channel_form_map.project_id',$request->id)
            ->get()->toArray();


        return json_encode($data);

//        return view('admin.forms.project_form_map');
    }

    public function project_channel_form_map_callback(Request $request)
    {


             $data['form_map'] = DB::table('project_channel_form_map')
                 ->select('form.form_fields','form.id')
                 ->leftJoin('form','form.id','=','project_channel_form_map.form_id')
                 ->where('project_channel_form_map.project_id',$request->project_id)
                 ->where('project_channel_form_map.channel_id',$request->channel_id)
                 ->get()->toArray();


             $data['form_data'] = DB::table('form_data')
                 ->select('form_data.id','form_data.data')
                 ->leftJoin('form','form.id','=','form_data.form_id')
                 ->where('form_data.form_id',$data['form_map'][0]->id)
                 ->get();


             return json_encode($data);


    }



        /*
         * create project map
         *
         * */
    public function create_project_form_map()
    {
        $data['projects'] = Projects::all();
        $data['forms'] = Form::all();
        return view('admin.forms.create_project_form_map',compact('data'));
    }

        /*
         * Manage Project Map
         *
         * */
    public function project_form_map()
    {
        $forms = DB::table('project_channel_form_map')
            ->select('projects.name as project_name','channels.name as channel_name',
                'form.title as form_title','project_channel_form_map.id')
            ->leftJoin('channels','channels.id','=','project_channel_form_map.channel_id')
            ->leftJoin('projects','projects.id','=','project_channel_form_map.project_id')
            ->leftJoin('form','form.id','=','project_channel_form_map.form_id')
            ->get();

        return view('admin.forms.project_form_map',compact('forms'));
    }

    public function edit_project_form_map($id)
    {
        $data['form_map']=  ProjectChannelFormMap::find($id);
        $data['projects'] = Projects::all();
        $data['forms'] = Form::all();
        return view('admin.forms.edit_project_form_map',compact('data'));
    }


    public function form_get()
    {
//
        $data['form_data'] = DB::table('form_data')
            ->select('form_data.id','form.title','form_data.data','form.form_fields',
                'form_data.project_id as project_id','projects.name as project_name',
                'form_data.channel_id as channel_id','form_data.city_id as city_id',
                'cities.name as city_name','channels.name as channel_name',
                'users.name as user_name','teams.name as team_name')
            ->leftJoin('form', 'form.id', '=', 'form_data.form_id')
            ->leftJoin('users', 'users.id', '=', 'form_data.user_id')
            ->leftJoin('cities', 'cities.id', '=', 'form_data.city_id')
            ->leftJoin('channels', 'channels.id', '=', 'form_data.channel_id')
            ->leftJoin('projects', 'projects.id', '=', 'form_data.project_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','users.id')
            ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id')
            ->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();


        return view('admin.forms.manage_form_data',compact('data'));
    }



    public function form_get_search(Request $request)
    {

//
       $form = Form_data::
            select('form_data.id','form.title','form_data.data','form.form_fields',
                'form_data.project_id as project_id','projects.name as project_name',
                'form_data.channel_id as channel_id','form_data.city_id as city_id',
                'cities.name as city_name','channels.name as channel_name',
                'users.name as user_name','teams.name as team_name')
            ->leftJoin('form', 'form.id', '=', 'form_data.form_id')
            ->leftJoin('users', 'users.id', '=', 'form_data.user_id')
            ->leftJoin('cities', 'cities.id', '=', 'form_data.city_id')
            ->leftJoin('channels', 'channels.id', '=', 'form_data.channel_id')
            ->leftJoin('projects', 'projects.id', '=', 'form_data.project_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','users.id')
            ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id');

             if($request->project){
                 $form->where('form_data.project_id',$request->project);
             }
             if($request->city){
                 $form->where('form_data.city_id',$request->city);
             }
             if($request->channel){
                 $form->where('form_data.channel_id',$request->channel);
             }

        $data['form_data']  = $form->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('admin.forms.manage_form_data',compact('data'));

    }

    public function form_data_view($id)
    {

        $form_data = DB::table('form_data')
            ->where('id',$id)
            ->orderBy('id','desc')
            ->get();

        foreach($form_data as $key):
            $data['id'] = $key->id;
            $data['data'] = json_decode($key->data);
        endforeach;

       return view('admin.forms.view_form_data',compact('data'));
    }



    public function form_data_report()
    {

        $form = DB::table('form_data')
            ->select('agencies.name as agency_name','projects.name as project_name'
            ,'form_data.created_at as date','employees.name as employee_name'
            ,'form_data.channel_id','form_data.city_id as cities_id','form_data.lat',
                'form_data.log','employee_project_map.team_id' )
            ->leftJoin('projects','projects.id','=','form_data.project_id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('channels','channels.id','=','form_data.channel_id')
            ->leftJoin('cities','cities.id','=','form_data.city_id')
            ->leftJoin('employees','employees.id','=','form_data.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','form_data.user_id')
            ->orderBy('form_data.id','desc')
            ->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('admin.forms.form_report',compact('data','form'));

    }


    public function form_data_report_search(Request $request)
    {

       $pro = DB::table('form_data')
            ->select('agencies.name as agency_name','projects.name as project_name'
                ,'form_data.created_at as date','employees.name as employee_name'
                ,'form_data.channel_id','form_data.city_id as cities_id','form_data.lat',
                'form_data.log','employee_project_map.team_id')
            ->leftJoin('projects','projects.id','=','form_data.project_id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('channels','channels.id','=','projects.channel_id')
            ->leftJoin('cities','cities.id','=','projects.cities_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','form_data.user_id')
            ->leftJoin('employees','employees.id','=','form_data.user_id');
        if($request->agency){
            $pro->where('projects.agency_id',$request->agency);
        }
           if($request->project){
               $pro->where('form_data.project_id',$request->project);
           }
           if($request->employee){
               $pro->where('employees.id',$request->employee);
           }
           if($request->channel){
               $pro->where('form_data.channel_id',$request->channel);
           }
           if($request->city){
               $pro->where('form_data.city_id',$request->city);
           }
           $form = $pro->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();;
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('admin.forms.form_report',compact('data','form'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $form = Form::find($id);

        $form->is_public = 0;
        $form->save();

        return redirect('admin/forms');
    }

}
