<?php

namespace App\Http\Controllers\Admin;

use App\Channels;
use App\Employee;
use App\EmployeeProjectMap;
use App\Form;
use App\Form_data;
use App\Projects;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $count['users'] = User::all()->count();
        $count['channels'] = Channels::all();
        $count['forms'] = Form_data::all()->count();
        $count['projects'] = Projects::all()->count();

        $users = DB::table('employees')
            ->select('employees.id as employee_id','employee_project_map.channel_id',
                'users.id as user_id','channels.name as channel_name','employees.cnic',
                'employees.name as employee_name','employees.email','employees.status',
                'employees.image','employee_project_map.project_id','employee_project_map.team_id',
                'projects.name as project_name','teams.name as team_name')
            ->leftJoin('users','users.employee_id','=','employees.id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('teams','teams.id','=','employee_project_map.team_id')
            ->where('employees.status','pending')
            ->get()->toArray();


        return view('admin.dashboard', compact('count','users'));

    }

//    public function show_forms()
//    {
//        $forms = Form::all()->toArray();
//        return view('admin.manage_forms',compact('forms'));
//    }
//
//    public function view_forms($id)
//    {
//        $forms = Form::find($id);
//
//        return view('admin.view_forms',compact('forms'));
//    }


    public function logout()
    {
        auth()->logout();
        return redirect('admin/login');
    }


}
