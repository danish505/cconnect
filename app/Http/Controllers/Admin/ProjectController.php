<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use App\Agency_project_map;
use App\Brand_project_map;
use App\Brands;
use App\Channel_project_map;
use App\Channels;
use App\Cities;
use App\City_project_map;
use App\EmployeeProjectMap;
use App\Form;
use App\ProjectChannelFormMap;
use App\Projects;
use App\Team;
use App\Teams_project_map;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $projects = Projects::where('is_deleted',0)->orderBy('id','desc')->paginate(15);
        return view('admin.projects.manage_project',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['channels'] = Channels::all()->toArray();
        $data['cities'] = Cities::all()->toArray();;
        $data['brands'] = Brands::all()->toArray();;
        $data['teams'] = Team::all()->toArray();
        $data['agency'] = Agency::all()->toArray();

        return view('admin.projects.new_project',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'team' => 'required',
            'brand' => 'required',
            'channel' => 'required',
            'start_date'=>'required',
            'exp_date'=>'required'
        ]);

        $project = new Projects();

        $city = array_filter($request->city);

        $team = array_filter($request->team);

        $project->name = $request->name;
         
        
        
    
    
        $project->start_date = Carbon::createFromFormat('Y-m-d',$request->start_date);
        $project->expiry_date = Carbon::createFromFormat('Y-m-d',$request->exp_date)->setTime(23,59,0); 
         
        

        $project->brand_id = implode(',',$request->brand);
        $project->channel_id = implode(',',$request->channel);
        $project->cities_id = implode(',',$city);
        $project->agency_id =  implode(',',$request->agency);
        $project->status = $request->status;
        $project->created_ip = $request->ip();
        $project->save();
        $project_id = $project->id;

           for($i=0; $i<count($city); $i++) {
//
               foreach ($team[$i] as $key => $val) {

                   $team_array = ['project_id' => $project_id, 'team_id' => $val, 'city_id' => $city[$i]];
//
                   Teams_project_map::create($team_array);
//
               }
           }

        $data['channel'] = $request->channel;

        foreach($data['channel'] as $key):

        $channel = ['project_id'=>$project_id,'channel_id'=>$key];
        Channel_project_map::create($channel);

        endforeach;

        $data['cities'] = $request->city;

        foreach($data['cities'] as $cit):
//
        $cities = ['project_id'=>$project_id,'city_id'=>$cit];

        City_project_map::create($cities);
//
        endforeach;

        $data['brand'] = $request->brand;

        foreach($data['brand'] as $brand):

            $brand = ['project_id'=>$project_id,'brand_id'=>$brand];

            Brand_project_map::create($brand);

        endforeach;
        $data['agency'] = $request->agency;
        foreach($data['agency'] as $agency):

            $agency = ['project_id'=>$project_id,'agency_id'=>$agency];
            Agency_project_map::create($agency);

        endforeach;

        return redirect('admin/projects/');



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $project = Projects::where('id',$id)->where('is_deleted',0)->get();

       return view('admin.projects.edit_project',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $project = Projects::find($id);


        $project->start_date = Carbon::createFromFormat('Y-m-d',$request->start_date);
        $project->expiry_date = Carbon::createFromFormat('Y-m-d',$request->exp_date)->setTime(23,59,0);
        $project->save();


        return redirect('admin/projects');


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit_form_map_project_id($id)
    {
          $data['project'] = DB::table('projects')
            ->select('projects.id','projects.brand_id','projects.channel_id',
                'projects.cities_id','projects.name','projects.start_date',
                'projects.expiry_date','projects.agency_id','projects.status as project_status')
            ->where('projects.id',$id)
            ->get();
            
           

        $data['channels']= DB::table('channels')
            ->whereIn('channels.id',explode(',',$data['project'][0]->channel_id))
            ->get();

        foreach($data['channels'] as $key):
//
        $map = ProjectChannelFormMap::select('project_id','channel_id')
            ->where('project_channel_form_map.project_id',$id)
            ->where('project_channel_form_map.channel_id',$key->id)
            ->get()->toArray();
            $project_map[] =  $map;

        endforeach;



        return view('admin.projects.edit_formmap',compact('data','project_map'),['id'=>$id]);


    }
    

    public function create_or_edit_form($project_id,$id)
    {
        //
        $form_get = DB::table('channels')
            ->leftJoin('form','form.id','=','channels.form_id')
            ->where('channels.id',$id)
            ->get()->toArray();

        $form = DB::table('project_channel_form_map')
            ->leftJoin('form','form.id','=','project_channel_form_map.form_id')
            ->where('project_channel_form_map.channel_id',$id)
            ->where('project_channel_form_map.project_id',$project_id)
            ->limit(1)
            ->orderBy('project_channel_form_map.id','desc')
            ->get()->toArray();


        if($form):
            return view('admin.projects.edit_forms',compact('form'),['id'=>$id,'project_id'=>$project_id]);

        elseif($form_get[0]->form_fields == true):

        return view('admin.projects.edit_forms',['form'=>$form_get,'id'=>$id,'project_id'=>$project_id]);

        else:
            echo "true";
         endif;

    }

    public function project_form_store(Request $request)
    {
        //
        $channel = DB::table('channels')
            ->where('id',$request->id)
            ->get();

        $project = Project::find($request->project_id);

        $input = strip_tags($request->formStructure);

        $output = json_decode($input);


        $form = new Form();

        $form->user_id = auth()->user()->id;
        $form->title = " $project[0]->name ({$channel[0]->name})";
        $form->form_fields = $output;
        $form->form_settings = 1;
        $form->is_public = 1;
        $form->status = 'approved';
        $form->created_ip = $request->ip();
        $form->save();
        $form_id = $form->id;

        $pcfm = new ProjectChannelFormMap();

        $pcfm->project_id = $request->project_id;
        $pcfm->channel_id = $request->id;
        $pcfm->form_id = $form_id;
        $pcfm->save();

    }
    
    public function update_project_form(Request $request,$id)
    {
       
         $channel = DB::table('channels')
             ->where('id',$id)
             ->get();

        $form = DB::table('project_channel_form_map')
            ->where('project_channel_form_map.channel_id',$id)
            ->where('project_channel_form_map.project_id',$request->project_id)
            ->limit(1)
            ->orderBy('project_channel_form_map.id','desc')
            ->get()->toArray();



        $input = strip_tags($request->formStructure);
        $output = json_decode($input);

        if($form):
            $form_id =  $form[0]->form_id;

            $forms  = Form::find($form_id);
            $forms->user_id = auth()->user()->id;
            $forms->form_fields =  $output;
            $forms->updated_ip = $request->ip();
            $forms->save();

        $pcfm = ProjectChannelFormMap::find($form[0]->id);

        $pcfm->project_id = $request->project_id;
        $pcfm->channel_id = $id;
        $pcfm->form_id = $form_id;
        $pcfm->save();

        return response()->json("success form");

        elseif($channel):

        $form = new Form();

            $project = DB::table('projects')
                ->where('id',$request->project_id)
                ->get();

        $form->title =   $project[0]->name ."({$channel[0]->name})";
        $form->user_id = auth()->user()->id;
        $form->form_fields =  $output;
        $form->updated_ip = $request->ip();
        $form->save();
        $org_from_id = $form->id;

        $pcfm = new ProjectChannelFormMap();

        $pcfm->project_id = $request->project_id;
        $pcfm->channel_id = $id;
        $pcfm->form_id = $org_from_id;
        $pcfm->save();

            return response()->json("success from channel form");

        endif;


    }

    public function project_user_status()
    {

        $users = DB::table('employees')
            ->select('employee_project_map.employee_id','employee_project_map.project_id',
                'employee_project_map.channel_id','employee_project_map.city_id','employee_project_map.brand_id',
                'employees.name as employee_name','employee_project_map.status','projects.name as project_name',
                'employees.cnic','channels.name as channel_name')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->where('employee_project_map.status',0)
            ->get();

        return view('admin.users.projects_status',compact('users'));

    }


    public function project_user_status_update(Request $request, $id)
    {

        $update =  DB::table('employee_project_map')
            ->where('employee_id',$id)->update(['status'=>$request->status]);

        return redirect('admin/projects/status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $project = Projects::find($id);

        if($project->is_deleted==0){
            $project->is_deleted = 1;
            $project->status = 0;
            $project->save();
        }
         return redirect('admin/projects');

    }
}
