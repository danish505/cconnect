<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use App\Brands;
use App\Channels;
use App\Cities;
use App\Projects;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        //
        $agency = Agency::orderBy('id','desc')->paginate(15);
        return view('admin.agency.manage_agency',compact('agency'));
    }


    public function onground_activation()
    {

        $projects = DB::table('agencies')
            ->select('projects.brand_id','projects.channel_id','projects.team_id','projects.cities_id',
                'projects.agency_id', 'projects.brand_id','projects.name','projects.start_date','projects.expiry_date')
            ->leftJoin('agency_project_map','agency_project_map.agency_id','=','agencies.id')
            ->leftJoin('projects','projects.id','=','agency_project_map.project_id')
            ->whereDate('projects.expiry_date','>',Carbon::today())
            ->paginate(15);

        $data['brand'] = Brands::all();
        $data['project'] = Projects::all();
        $data['team'] = Team::all();
        $data['cities'] = Cities::all();
        $data['agencies'] = Agency::all();
        $data['channels'] = Channels::all();

        return view('admin.agency.onground_activation',compact('projects','data'));
    }


    public function onground_activation_search(Request $request)
    {


        $pro =  $projects = DB::table('agencies')
            ->select('projects.brand_id','projects.channel_id','projects.team_id','projects.cities_id',
                'projects.agency_id', 'projects.brand_id','projects.name','projects.start_date','projects.expiry_date')
            ->leftJoin('agency_project_map','agency_project_map.agency_id','=','agencies.id')
            ->leftJoin('projects','projects.id','=','agency_project_map.project_id')
            ->whereDate('projects.expiry_date','>',Carbon::today());

            if($request->project){
                $pro->where('projects.id',$request->project);
            }
            if($request->agency){
                $pro->where('projects.agency_id',$request->agency);
            }

            $projects = $pro->paginate(15);


        $data['brand'] = Brands::all();
        $data['project'] = Projects::all();
        $data['team'] = Team::all();
        $data['cities'] = Cities::all();
        $data['agencies'] = Agency::all();
        $data['channels'] = Channels::all();

        return view('admin.agency.onground_activation',compact('projects','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.agency.create_agency');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $agency = new Agency();

        $agency->name = $request->agency;
        $agency->address = $request->address;
        $agency->email= $request->email;
        $agency->phone= $request->phone;
        $agency->created_ip = $request->ip();
        $agency->save();

        return redirect('admin/agency');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        $agency = Agency::find( $id);

        return view('admin.agency.edit_agency',compact('agency'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $agency = Agency::find( $id);

        $agency->name = $request->agency;
        $agency->address = $request->address;
        $agency->email= $request->email;
        $agency->phone= $request->phone;
        $agency->updated_ip = $request->ip();
        $agency->save();

        return redirect('admin/agency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $agency = Agency::find($id);

        $agency->delete();
        return redirect('admin/agency');
    }
}
