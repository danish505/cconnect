<?php

namespace App\Http\Controllers\Admin;

use App\Channels;
use App\Form;
use App\Form_data;
use App\ProjectChannelFormMap;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        //
        $channels = Channels::orderBy('id','desc')->paginate(15);
        return view('admin.channels.manage',compact('channels'));

    }

    public function edit_form_template($id)
    {
        $form = Form::find($id);

        return view('admin.channels.channel_form_edit',compact('form'));

    }

    public function update_form_channel(Request $request,$id)
    {
        $channel = DB::table('channels')->where('form_id',$id)->get();

        $input = strip_tags($request->formStructure);
        $output = json_decode($input);

        $form  = Form::find($id);
        $form->title = $channel[0]->name.' Form Template';
        $form->form_fields =  $output;
        $form->updated_ip = $request->ip();
        $form->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $forms = Form::all()->toArray();
        return view('admin.channels.create',compact('forms'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $channel = new Channels();

        $channel->name = $request->name;
        $channel->created_ip = $request->ip();
        $channel->save();
        $id = $channel->id;

        return redirect("admin/channels/form/create/{$id}");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function create_form_template($id)
    {

        return view('admin.channels.create_channel_form',['id'=>$id]);

    }



    public function store_form_template(Request $request)
    {
        $form = new Form();

        $channel = DB::table('channels')
            ->where('id',$request->id)
            ->get();

         if($channel):
            $title = $channel[0]->name." Form Template";
            $form->title = $title;
            $form->form_fields = $request->formStructure;
            $form->user_id = auth()->user()->id;
            $form->status = 1;
            $form->is_public = 1;
            $form->save();

            $form_id =  $form->id;

            $chann = Channels::find($request->id);
            $chann->form_id = $form_id;
            $chann->save();

            return response()->json("success",200);

         endif;

            return response()->json("Error Occured",400);
    }


    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $channel = DB::table('channels')
            ->select('channels.id as channel_id','form.title','channels.name','form.id as form_id')
            ->leftJoin('form','form.id','=','channels.form_id')
            ->where('channels.id',$id)
            ->limit(1)
            ->get();

        return view('admin.channels.edit',compact('channel'));

    }
    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $channel = Channels::find($id);
        $channel->name = $request->name;
        $channel->updated_ip = $request->ip();

        $channel->save();

        return redirect('admin/channels');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $channel = Channels::find($id);
        $channel->delete();
        return redirect('admin/channels');

    }
}
