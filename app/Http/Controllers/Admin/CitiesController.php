<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use App\Brands;
use App\Channels;
use App\Cities;
use App\Projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        //
        $cities = Cities::orderBy('id','desc')->paginate();

        return view('admin.cities.manage_cities',compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $city = new Cities();
        $city->name = $request->city;
        $city->created_ip = $request->ip();
        $city->save();
        return redirect('admin/cities');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = Cities::find($id);

        return view('admin.cities.edit_city',compact('city'));

    }



    public function online_cities()
    {
         $cities = DB::table('cities')
            ->select('cities.name as city_name','agencies.name as agency_name',
                'projects.name as project_name','projects.start_date','projects.expiry_date','brands.name as brand_name')
            ->leftJoin('city_project_map','city_project_map.city_id','=','cities.id')
            ->leftJoin('projects','projects.id','=','city_project_map.project_id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('brands','brands.id','=','projects.brand_id')
            ->whereDate('projects.expiry_date','>',Carbon::today())
            ->orderBy('city_project_map.id','desc')
            ->paginate(15);

        $data['cities'] = Cities::all();
        $data['project'] = Projects::all();
        $data['projects'] = Projects::all();
        $data['agency'] = Agency::all();
        $data['brand'] = Brands::all();


        return view('admin.cities.online_cities',compact('cities','data'));
    }



    public function online_cities_search(Request $request)
    {
         $pro = DB::table('cities')
            ->select('cities.name as city_name','agencies.name as agency_name',
                'projects.name as project_name','projects.start_date','projects.expiry_date','brands.name as brand_name')
            ->leftJoin('city_project_map','city_project_map.city_id','=','cities.id')
             ->leftJoin('projects','projects.id','=','city_project_map.project_id')
             ->leftJoin('brands','brands.id','=','projects.brand_id')
             ->leftJoin('brand_project_map','brand_project_map.brand_id','=','brands.id')
             ->leftJoin('agencies','agencies.id','=','projects.agency_id')
             ->leftJoin('agency_project_map','agency_project_map.agency_id','=','agencies.id')
            ->whereDate('projects.expiry_date','>',Carbon::today())
            ->orderBy('city_project_map.id','desc');
         if($request->city){
             $pro->where('city_project_map.city_id',$request->city);
         }
         if($request->brand) {
             $pro->where('brand_project_map.brand_id', $request->brand);

         }
         if($request->agency){
             $pro->where('agency_project_map.agency_id',$request->agency);
         }
         if($request->project){
             $pro->where('projects.id',$request->project);
         }
         $cities = $pro->paginate(15);

        $data['projects'] = Projects::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::all();
        $data['agency'] = Agency::all();
        $data['brand'] = Brands::all();


        return view('admin.cities.online_cities',compact('cities','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $city = Cities::find($id);

        $city->name = $request->city;
        $city->updated_ip = $request->ip();
        $city->save();
        return redirect('admin/cities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $city = Cities::find($id);

        $city->delete();

        return redirect('admin/cities');

    }
}
