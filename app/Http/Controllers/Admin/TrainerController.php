<?php

namespace App\Http\Controllers\Admin;

use App\Trainers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        //
        $trainer = Trainers::paginate(15);
        return view('admin.trainers.manage_trainer',compact('trainer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.trainers.create_trainer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'password'=>'required|min:6|confirmed',
            'password_confirmation'=>'required|min:6',
            'phone'=>'required',
            'cnic'=>'required|unique:users,cnic',
            'email'=>'required',
            'photo'=>'required',
            'name'=>'required',
        ]);

        $trainer = new Trainers();

        $trainer->name = $request->name;
        $trainer->fathername = $request->fname;
        $trainer->email = $request->email;
        $trainer->cnic = $request->cnic;
        $trainer->is_deleted = 0;
        if($request->hasFile('photo')){

            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/trainer/',$filename);
            $trainer->image = $filename;
        }
        $trainer->cell = $request->phone;

        $trainer->save();
       $trainer_id =  $trainer->id;

       $user = new User();

       $user->name = $request->name;
       $user->username = $request->cnic;
       $user->password = Hash::make($request->password);
       $user->type= 2;
        $user->employee_id = $trainer_id;
       $user->created_ip = $request->ip();
       $user->save();

       return redirect('admin/trainers/');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
