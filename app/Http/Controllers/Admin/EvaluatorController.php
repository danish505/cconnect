<?php

namespace App\Http\Controllers\Admin;

use App\Evaluators;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class EvaluatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $evaluator = Evaluators::paginate(15);
        return view('admin.evaluator.manage_evaluator',compact('evaluator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.evaluator.create_evaluator');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'password'=>'required|min:6|confirmed',
            'password_confirmation'=>'required|min:6',
            'phone'=>'required',
            'cnic'=>'required|unique:users',
            'email'=>'required',
            'photo'=>'required',
            'name'=>'required',
        ]);

        $evaluator = new Evaluators();

        $evaluator->name = $request->name;
        $evaluator->fathername = $request->fname;
        $evaluator->email = $request->email;
        $evaluator->cnic = $request->cnic;
        if($request->hasFile('photo')){

            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/evaluator/',$filename);
        }
        $evaluator->image = $filename;
        $evaluator->cell = $request->phone;
        $evaluator->is_deleted = 0;

        $evaluator->save();
        $evaluator_id =  $evaluator->id;

        $user = new User();

        $user->name = $request->name;
        $user->username = $request->cnic;
        $user->password = Hash::make($request->password);
        $user->type= 2;
        $user->employee_id = $evaluator_id;
        $user->created_ip = $request->ip();
        $user->save();

        return redirect('admin/evaluator/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
