<?php

namespace App\Http\Controllers\Evaluator;

use App\EvaluatorFormData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:trainer');
    }

    public function index()
    {
        //



        $evaluator = EvaluatorFormData::select('evaluators.name','evaluators.cnic','evaluators_form_data.form_data',
            'projects.name as project_name','channels.name as channel_name')
            ->leftJoin('evaluators','evaluators.id','=','evaluators_form_data.evaluator_id')
            ->leftJoin('channels','channels.id','=','evaluators_form_data.channel_id')
            ->leftJoin('projects','projects.id','=','evaluators_form_data.project_id')
            ->leftJoin('evaluator_form','evaluator_form.id','=','evaluators_form_data.evaluator_form_id')
            ->paginate(15);
        return view('evaluators.dashboard',compact('evaluator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function evaluator_form_data()
    {
        $data = DB::table('evaluators_form_data')
            ->select('evaluators_form_data.id','evaluator_form.title','evaluators_form_data.form_data',
                'evaluator_form.form_fields','evaluators_form_data.project_id as project_id','projects.name as project_name',
                'evaluators_form_data.channel_id as channel_id','channels.name as channel_name',
                'evaluators.name as user_name')
            ->leftJoin('evaluator_form', 'evaluator_form.id', '=', 'evaluators_form_data.evaluator_form_id')
            ->leftJoin('evaluators', 'evaluators.id', '=', 'evaluators_form_data.evaluator_id')
            ->leftJoin('channels', 'channels.id', '=', 'evaluators_form_data.channel_id')
            ->leftJoin('projects', 'projects.id', '=', 'evaluators_form_data.project_id')
            ->paginate(15);

        return view('evaluators.manage_form_data',compact('data'));
    }

    public function evaluator_form_data_view($id)
    {
        $form_data = DB::table('evaluators_form_data')
            ->where('id',$id)
            ->orderBy('id','desc')
            ->get();

        $data = array();

        foreach($form_data as $key):
            $data['id'] = $key->id;
            $data['data'] = json_decode($key->form_data);
        endforeach;
         

        return view('evaluators.form_data_view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function logout()
    {
        auth()->logout();
        return redirect('evaluator/login');
    }
}
