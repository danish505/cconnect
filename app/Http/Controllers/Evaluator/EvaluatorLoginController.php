<?php

namespace App\Http\Controllers\Evaluator;

use App\Evaluators;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class EvaluatorLoginController extends Controller
{
    //

    public function __construct()
    {
    $this->middleware('guest:trainer');
    }

    public function LoginForm()
    {
        return view('evaluators.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'cnic' => 'required|min:13',
            'password' => 'required|min:6'
        ]);

        $evaluator = Evaluators::where('cnic', $request->cnic)->first();

        if ($evaluator) {


            if (Auth::guard('trainer')->attempt(['username' => $request->cnic, 'password' => $request->password], $request->remember)) {

                return redirect()->intended(route('evaluator.dashboard'));
            }

            return redirect()->back()->withInput($request->only('cnic', 'remember'));
        }

    }
}

