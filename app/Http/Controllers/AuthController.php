<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Evaluators;
use App\Trainers;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    //


    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        if (request('username') == '' || request('password') == '' || request('lat') == '' || request('log') == '') {
            $message = array(

                'message' => 'Fill out all fields',
                'status' => '0'
            );
            return json_encode($message);
        }
        $credentials = request(['username', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $employee = Employee::where('cnic', request('username'))->first();

        $trainer = Trainers::where('cnic',request('username'))->first();
        $evaluator = Evaluators::where('cnic',request('username'))->first();

        if($trainer || $evaluator)
        {

            $users = DB::table('users')->where('username', request('username'))->get();

            foreach ($users as $user):

                $session = DB::table('login_session')
           ->insert(['user_id' => $user->id, 'lat' => request('lat'), 'log' => request('log'),'created_ip'=>request()->ip()]);

            endforeach;

            return $this->respondWithToken($token);

        }

        if($employee){

            if($employee->status == 'pending' || $employee->status == 'disapproved') {
                $message = array(
                    'message' => 'You can`t login',
                    'user_type'=>1,
                    'employee_status' => $employee->status
                );
                return json_encode($message);

            }
            $user = DB::table('users')->where('username', request('username'))->first();

            $session = DB::table('login_session')
                ->insert(['user_id' => $user->id, 'lat' => request('lat'),
                    'log' => request('log'),'created_ip'=>request()->ip()]);

            return $this->respondWithToken($token);


        }


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {

        if(auth()->user()->type == 1){

        $employee_image = DB::table('employee_project_map')
            ->select('employees.image', 'employees.name', 'employees.status as employee_status',
                'employee_project_map.project_id', 'employee_project_map.channel_id', 'employee_project_map.city_id',
                'employee_project_map.brand_id', 'employee_project_map.team_id', 'projects.expiry_date',
                'employee_project_map.status')
            ->leftJoin('employees', 'employee_project_map.employee_id', '=', 'employees.id')
            ->leftJoin('users', 'users.employee_id', '=', 'employees.id')
            ->leftJoin('projects', 'projects.id', '=', 'employee_project_map.project_id')
            ->where('employees.id', auth()->user()->employee_id)
            ->orderBy('employee_project_map.id','desc')
            ->limit(1)
            ->get();



            $clock = DB::table('clock')
                ->where('user_id', auth()->user()->id)
                ->whereDate('clock_in', Carbon::today())
                ->limit(1)->orderBy('id', 'desc')
                ->get()->toArray();

            $break = DB::table('break')
                ->where('user_id', auth()->user()->id)
                ->whereDate('break_in', Carbon::today())
                ->limit(1)->orderBy('id', 'desc')
                ->get()->toArray();


            foreach($employee_image as $key):

                $clock_data = null;
                $break_data = null;
                $channel_name = null;
                $channel_ids = null;

                $channels = DB::table('channels')
                    ->select('id', 'name')
                    ->whereIn('id', explode(',', $key->channel_id))
                    ->get()->toArray();

                $is_expired = false;
                if ($key->expiry_date <= Carbon::now()) {
                    $is_expired = true;
                }

                if ($clock && sizeof($clock) > 0) {
                    $clock_data = $clock[0];
                }
                if ($break && sizeof($break) > 0) {
                    $break_data = $break[0];
                }
                if ($channels && sizeof($channels) > 0) {

                    $arraynames = array();
                    $arrayids = array();

                    foreach ($channels as $val):
                        $arraynames[] = $val->name;
                        $arrayids[] = $val->id;
                    endforeach;
                    $channel_name = implode(",", $arraynames);
                    $channel_ids = implode(",", $arrayids);
                }


                return response()->json([
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => auth()->factory()->getTTL() * 60,
                    'user_id' => auth()->user()->id,
                    'user_type'=> auth()->user()->type,
                    'name' => $key->name,
                    'image_url' => asset('uploads/employee/')."/{$key->image}",
                    'brand_id' => $key->brand_id,
                    'channel_id' => $channel_ids,
                    'city_id' => $key->city_id,
                    'team_id' => $key->team_id,
                    'project_id' => $key->project_id,
                    'expiry_project' => $key->expiry_date,
                    'is_expired' => $is_expired,
                    'break_data' => $break_data,
                    'clock_data' => $clock_data,
                    'channel_name' => $channel_name,
                    'project_status' => $key->status,
                    'employee_status'=>$key->employee_status

                ]);

            endforeach;

        }
        if(auth()->user()->type == 2){


            $trainers = Trainers::select('trainers.image', 'trainers.name', 'trainers.status as trainer_status')
                ->where('trainers.id', auth()->user()->employee_id)
                ->first();


            $arr_filter = array(
                'access_token' => $token,
                'user_id' => auth()->user()->id,
                'user_type'=> auth()->user()->type,
                'image_url'=>asset('uploads/trainer/')."/{$trainers->image}",
                'name'=>$trainers->name,

            );

            return response()->json($arr_filter);



        }
        if(auth()->user()->type== 3){

            $evaluators = DB::table('evaluators')
                ->select('evaluators.image', 'evaluators.name', 'evaluators.status as evaluators_status')
                ->where('evaluators.id', auth()->user()->employee_id)
                ->first();

            $arr_clean = array(
                'access_token' => $token,
                'user_id' => auth()->user()->id,
                'user_type'=> auth()->user()->type,
                'image_url'=>asset('uploads/evaluators/')."/{$evaluators->image}",
                'name'=>$evaluators->name,


            );
            return response()->json($arr_clean);

        }





    }

}
