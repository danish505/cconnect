<?php

namespace App\Http\Controllers\Client;

use App\Agency;
use App\Brands;
use App\Channels;
use App\Cities;
use App\Projects;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AgencyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function onground_activation()
    {

        $projects = DB::table('agencies')
            ->select('projects.brand_id','projects.channel_id','projects.team_id','projects.cities_id',
                'projects.agency_id', 'projects.brand_id','projects.name','projects.start_date','projects.expiry_date')
            ->leftJoin('agency_project_map','agency_project_map.agency_id','=','agencies.id')
            ->leftJoin('projects','projects.id','=','agency_project_map.project_id')
            ->whereDate('projects.expiry_date','>',Carbon::today())
            ->paginate(15);

        $data['brand'] = Brands::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['team'] = Team::all();
        $data['cities'] = Cities::all();
        $data['agencies'] = Agency::all();
        $data['channels'] = Channels::all();

        return view('client.agency.onground_activation',compact('projects','data'));
    }


    public function onground_activation_search(Request $request)
    {


        $pro =  $projects = DB::table('agencies')
            ->select('projects.brand_id','projects.channel_id','projects.team_id','projects.cities_id',
                'projects.agency_id', 'projects.brand_id','projects.name','projects.start_date','projects.expiry_date')
            ->leftJoin('agency_project_map','agency_project_map.agency_id','=','agencies.id')
            ->leftJoin('projects','projects.id','=','agency_project_map.project_id')
            ->whereDate('projects.expiry_date','>',Carbon::today());

        if($request->project){
            $pro->where('projects.id',$request->project);
        }
        if($request->agency){
            $pro->where('projects.agency_id',$request->agency);
        }

        $projects = $pro->paginate(15);


        $data['brand'] = Brands::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['team'] = Team::all();
        $data['cities'] = Cities::all();
        $data['agencies'] = Agency::all();
        $data['channels'] = Channels::all();

        return view('client.agency.onground_activation',compact('projects','data'));
    }


}
