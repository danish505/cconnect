<?php

namespace App\Http\Controllers\Client;


use App\Agency;
use App\Brands;
use App\Channel_project_map;
use App\Channels;
use App\Cities;
use App\Employee;
use App\EmployeeProjectMap;
use App\Projects;
use App\Team;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function online_users()
    {
        $employees = DB::table('clock')
            ->select('employees.id as employee_id','employees.name as employee_name','employees.email','clock.clock_in',
                'employees.name','brands.id as brand_id','brands.name as brand_name',
                'cities.name as city_name','channels.name as channel_name','agencies.name as agency_name',
                'projects.name as project_name','projects.expiry_date','projects.id as project_id',
                'clock.channel_id','clock.clock_out','clock.clockin_image','clock.clockout_image',
                'clock.clockin_lat','clock.clockin_log')
            ->leftJoin('employees','employees.id','=','clock.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','clock.project_id')
            ->leftJoin('channels','channels.id','=','clock.channel_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
//            ->leftJoin('break','break.user_id','=','employees.id')
            ->whereDate('clock.clock_in',Carbon::today())
            ->whereNull('clock_out')
            ->whereDate('projects.expiry_date','>=',Carbon::today())
            ->orderBy('clock.created_at')
            ->orderBy('employee_project_map.created_at')
            ->paginate(15);







        $data['channels']= Channels::all();
        $data['projects'] = Projects::all();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
        $data['city'] = Cities::all();

        return view('client.users.online_users_report',compact('employees','data'));
    }

    public function online_users_search(Request $request)
    {
        $emp = DB::table('clock')
            ->select('employees.id as employee_id','employees.name as employee_name','employees.email','clock.clock_in',
                'employees.name','brands.id as brand_id','brands.name as brand_name',
                'cities.name as city_name','channels.name as channel_name','agencies.name as agency_name',
                'projects.name as project_name','projects.expiry_date','projects.id as project_id',
                'clock.channel_id','clock.clock_out','clock.clockin_image','clock.clockout_image',
                'clock.clockin_lat','clock.clockin_log')
            ->leftJoin('employees','employees.id','=','clock.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','clock.project_id')
            ->leftJoin('channels','channels.id','=','clock.channel_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('break','break.user_id','=','clock.user_id')
            ->whereDate('clock.clock_in',Carbon::today())
            ->whereNull('clock_out')
            ->orderBy('clock.id','desc');
        if($request->employee){
            $emp->where('clock.user_id',$request->employee);
        }
        if($request->channel){
            $emp->where('clock.channel_id',$request->channel);
        }
        if($request->project){
            $emp->where('clock.project_id',$request->project);
        }

        $employees = $emp->paginate(15);

        // $break_array = array();

        // foreach($employees as $pro):
        //     $last_login = Employee::select('break.break_in','break.break_out','employees.id as user_id','break.lat','break.log')
        //         ->leftJoin('break','break.user_id','=','employees.id')
        //         ->where('break.user_id',$pro->employee_id)
        //         ->whereDate('break.break_in',Carbon::today())
        //         ->orderBy('break.id','desc')
        //         ->first();


        //     $break_array[] = $last_login;
        //     endforeach;




        $data['channels']= Channels::all();
        $data['projects'] = Projects::where('is_deleted',0)->get();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
        $data['city'] = Cities::all();

        return view('client.users.online_users_report',compact('employees','data'));

    }

    public function register_user()
    {


//        $project_array = array();
        $login_array = array();
//
        $projects = DB::table('projects')
            ->select('projects.name as project_name','projects.expiry_date','projects.id')
            ->orderBy('projects.id', 'desc')
            ->get();
    

            $users = DB::table('employees')
                ->select('employee_project_map.project_id',
                    'cities.name as city_name', 'agencies.name as agency_name', 'brands.name as brand_name', 'employee_project_map.team_id', 'employee_project_map.channel_id', 'employees.name as username', 'employees.created_at', 'employees.name as emp_name', 'employees.id as employee_id', 'employee_project_map.agency_id', 'employee_project_map.brand_id', 'employees.cnic' )
                ->leftJoin('employee_project_map', 'employee_project_map.employee_id', '=', 'employees.id')
                ->leftJoin('cities', 'cities.id', '=', 'employee_project_map.city_id')
                ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id')
                ->leftJoin('brands', 'brands.id', '=', 'employee_project_map.brand_id')
                ->leftJoin('agencies', 'agencies.id', '=', 'employee_project_map.agency_id')
                ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
                ->whereDate('projects.expiry_date', '>=', Carbon::today())
                ->paginate(15);

            foreach ($users as $pro):

                $last_login = DB::table('login_session')
                    ->select('lat', 'log', 'user_id', 'created_at')
                    ->where('login_session.user_id', $pro->employee_id)
                    ->orderBy('login_session.id','desc')
                    ->first();

                $login_array[] = $last_login;

            endforeach;
           

      

            $data['agency'] = Agency::all();
            $data['project'] = Projects::where('is_deleted',0)->get();
            $data['cities'] = Cities::all();
            $data['employee'] = Employee::all();
            $data['channels'] = Channels::all();

            return view('client.users.registered_users_report', compact('users', 'data', 'login_array','projects'));

        }





    public function register_user_search(Request $request)
    {
      $login_array = array();
$login_array = array();
//
        $projects = DB::table('projects')
            ->select('projects.name as project_name','projects.expiry_date','projects.id')
            ->orderBy('projects.id', 'desc')
            ->where('projects.expiry_date', '>', Carbon::today())
            ->get();


//        $emp_array = array();
        

            $reg_user = DB::table('employees')
                ->select('employee_project_map.project_id',
                    'cities.name as city_name', 'agencies.name as agency_name', 'brands.name as brand_name', 'employee_project_map.team_id', 'employee_project_map.channel_id', 'employees.name as username', 'employees.created_at', 'employees.name as emp_name', 'employees.id as employee_id', 'employee_project_map.agency_id', 'employee_project_map.brand_id', 'employees.cnic' )
                ->leftJoin('employee_project_map', 'employee_project_map.employee_id', '=', 'employees.id')
                ->leftJoin('cities', 'cities.id', '=', 'employee_project_map.city_id')
                ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id')
                ->leftJoin('brands', 'brands.id', '=', 'employee_project_map.brand_id')
                ->leftJoin('agencies', 'agencies.id', '=', 'employee_project_map.agency_id')
                ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
                ->whereDate('projects.expiry_date', '>=', Carbon::today());
                 
                 

            if($request->employee){
                $reg_user->where('employee_project_map.employee_id',$request->employee);
            }
            if($request->project){
                $reg_user->where('employee_project_map.project_id',$request->project);
            }
            if($request->city){
                $reg_user->where('employee_project_map.city_id',$request->city);
            }
            if($request->agency){
                $reg_user->where('employee_project_map.agency_id',$request->agency);
            }
            if($request->channel){
                $reg_user->where('employee_project_map.agency_id',$request->channel);
            }
            $users = $reg_user->paginate(15);
         
         foreach ($users as $pro):

                $last_login = DB::table('login_session')
                    ->select('lat', 'log', 'user_id', 'created_at')
                    ->where('login_session.user_id', $pro->employee_id)
                    ->orderBy('login_session.id','desc')
                    ->first();

                $login_array[] = $last_login;

            endforeach;
             


        $data['channels'] = Channels::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['cities'] = Cities::all();
        $data['employee'] = Employee::all();

        return view('client.users.registered_users_report',compact('users','data','login_array','projects'));

    }



    public function users_break()
    {
        //

        $users = DB::table('break')
            ->select('break.user_id','employees.name','break.break_in',
                'break.break_out','break.lat','break.log','break.created_at',
                'break.updated_at','agencies.name as agency_name','cities.name as city_name',
                'projects.name as project_name','channels.name as channel_name',
                'employee_project_map.channel_id','break.breakin_image','break.breakout_lat','break.breakout_log')
            ->leftJoin('employees','employees.id','=','break.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','employees.id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->whereDate('break.break_in',Carbon::today())
            ->whereNull('break.break_out')
            ->orderBy('break.id','desc')
            ->paginate(15);

        $data['channels']= Channels::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('client.users.users_breakin',compact('users','data'));

    }


    public function users_break_search(Request $request)
    {
        //

        $user_search = DB::table('break')
            ->select('break.user_id','employees.name','break.break_in',
                'break.break_out','break.lat','break.log','break.created_at',
                'break.updated_at','agencies.name as agency_name','cities.name as city_name',
                'projects.name as project_name','channels.name as channel_name',
                'break.channel_id','break.breakin_image',
                'break.breakout_lat','break.breakout_log')
            ->leftJoin('employees','employees.id','=','break.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','break.user_id')
            ->leftJoin('projects','projects.id','=','employee_project_map.project_id')
            ->leftJoin('channels','channels.id','=','employee_project_map.channel_id')
            ->leftJoin('brands','brands.id','=','employee_project_map.brand_id')
            ->leftJoin('cities','cities.id','=','employee_project_map.city_id')
            ->leftJoin('agencies','agencies.id','=','employee_project_map.agency_id')
            ->whereDate('break.break_in',Carbon::today())
            ->whereNull('break.break_out')
            ->orderBy('break.id','desc');

        if($request->agency){
            $user_search->where('employee_project_map.agency_id',$request->agency);
        }
        if($request->channel){
            $user_search->where('break.channel_id',$request->channel);
        }
        if($request->project){
            $user_search->where('break.project_id',$request->project);
        }
        if($request->employee){
            $user_search->where('break.user_id',$request->employee);
        }
        $users= $user_search->paginate(15);

        $data['channels']= Channels::all();
        $data['projects'] = Projects::all();
        $data['employees'] = Employee::all();
        $data['agency'] = Agency::all();
        $data['project'] = Projects::all();
//
        return view('client.users.users_breakin',compact('users','data'));
    }



}
