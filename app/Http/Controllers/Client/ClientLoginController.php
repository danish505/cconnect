<?php

namespace App\Http\Controllers\Client;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ClientLoginController extends Controller
{
    //


    public function __construct()
    {
        $this->middleware('guest:client');
    }

    public function LoginForm()
    {
        return view('client.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $client = Client::where('email', $request->email)->first();
            
            

        if ($client) {

            if (Auth::guard('web')->attempt(['username' => $request->email, 'password' => $request->password], $request->remember))
            {

                return redirect()->intended(route('client.dashboard'));
            }

            return redirect()->back()->withInput($request->only('email', 'remember'));
        }

    }



}
