<?php

namespace App\Http\Controllers\Client;

use App\Agency;
use App\Channels;
use App\Cities;
use App\Employee;
use App\Form;
use App\Form_data;
use App\ProjectChannelFormMap;
use App\Projects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class FormController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function form_data_report()
    {

        $form = DB::table('form_data')
            ->select('agencies.name as agency_name','projects.name as project_name'
                ,'form_data.created_at as date','employees.name as employee_name'
                ,'form_data.channel_id','form_data.city_id as cities_id','form_data.lat',
                'form_data.log','employee_project_map.team_id' )
            ->leftJoin('projects','projects.id','=','form_data.project_id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('channels','channels.id','=','form_data.channel_id')
            ->leftJoin('cities','cities.id','=','form_data.city_id')
            ->leftJoin('employees','employees.id','=','form_data.user_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','form_data.user_id')
            ->orderBy('form_data.id','desc')
            ->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();;
        ;
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('client.forms.form_report',compact('data','form'));

    }


    public function form_data_report_search(Request $request)
    {

        $pro = DB::table('form_data')
            ->select('agencies.name as agency_name','projects.name as project_name'
                ,'form_data.created_at as date','employees.name as employee_name'
                ,'form_data.channel_id','form_data.city_id as cities_id','form_data.lat',
                'form_data.log','employee_project_map.team_id')
            ->leftJoin('projects','projects.id','=','form_data.project_id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('channels','channels.id','=','projects.channel_id')
            ->leftJoin('cities','cities.id','=','projects.cities_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','form_data.user_id')
            ->leftJoin('employees','employees.id','=','form_data.user_id');
        if($request->agency){
            $pro->where('projects.agency_id',$request->agency);
        }
        if($request->project){
            $pro->where('form_data.project_id',$request->project);
        }
        if($request->employee){
            $pro->where('employees.id',$request->employee);
        }
        if($request->channel){
            $pro->where('form_data.channel_id',$request->channel);
        }
        if($request->city){
            $pro->where('form_data.city_id',$request->city);
        }
        $form = $pro->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('client.forms.form_report',compact('data','form'));

    }

    public function form_get()
    {
//
        $data['form_data'] = DB::table('form_data')
            ->select('form_data.id','form.title','form_data.data','form.form_fields',
                'form_data.project_id as project_id','projects.name as project_name',
                'form_data.channel_id as channel_id','form_data.city_id as city_id',
                'cities.name as city_name','channels.name as channel_name',
                'users.name as user_name','teams.name as team_name')
            ->leftJoin('form', 'form.id', '=', 'form_data.form_id')
            ->leftJoin('users', 'users.id', '=', 'form_data.user_id')
            ->leftJoin('cities', 'cities.id', '=', 'form_data.city_id')
            ->leftJoin('channels', 'channels.id', '=', 'form_data.channel_id')
            ->leftJoin('projects', 'projects.id', '=', 'form_data.project_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','users.id')
            ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id')
            ->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();


        return view('client.forms.manage_form_data',compact('data'));
    }



    public function form_get_search(Request $request)
    {

//
        $form = Form_data::
        select('form_data.id','form.title','form_data.data','form.form_fields',
            'form_data.project_id as project_id','projects.name as project_name',
            'form_data.channel_id as channel_id','form_data.city_id as city_id',
            'cities.name as city_name','channels.name as channel_name',
            'users.name as user_name','teams.name as team_name')
            ->leftJoin('form', 'form.id', '=', 'form_data.form_id')
            ->leftJoin('users', 'users.id', '=', 'form_data.user_id')
            ->leftJoin('cities', 'cities.id', '=', 'form_data.city_id')
            ->leftJoin('channels', 'channels.id', '=', 'form_data.channel_id')
            ->leftJoin('projects', 'projects.id', '=', 'form_data.project_id')
            ->leftJoin('employee_project_map','employee_project_map.employee_id','=','users.id')
            ->leftJoin('teams', 'teams.id', '=', 'employee_project_map.team_id');

        if($request->project){
            $form->where('form_data.project_id',$request->project);
        }
        if($request->city){
            $form->where('form_data.city_id',$request->city);
        }
        if($request->channel){
            $form->where('form_data.channel_id',$request->channel);
        }

        $data['form_data']  = $form->paginate(15);

        $data['channels'] = Channels::all();
        $data['cities'] = Cities::all();
        $data['project'] = Projects::where('is_deleted',0)->get();
        $data['employee'] = Employee::all();
        $data['agency'] = Agency::all();

        return view('client.forms.manage_form_data',compact('data'));

    }

    public function form_data_view($id)
    {

        $form_data = DB::table('form_data')
            ->where('id',$id)
            ->orderBy('id','desc')
            ->get();

        foreach($form_data as $key):
            $data['id'] = $key->id;
            $data['data'] = json_decode($key->data);
        endforeach;

        return view('client.forms.view_form_data',compact('data'));
    }


}
