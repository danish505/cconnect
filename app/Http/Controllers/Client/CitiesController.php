<?php

namespace App\Http\Controllers\Client;

use App\Agency;
use App\Brands;
use App\Channels;
use App\Cities;
use App\Projects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CitiesController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function online_cities()
    {
        $cities = DB::table('cities')
            ->select('cities.name as city_name','agencies.name as agency_name',
                'projects.name as project_name','projects.start_date','projects.expiry_date','brands.name as brand_name')
            ->leftJoin('city_project_map','city_project_map.city_id','=','cities.id')
            ->leftJoin('projects','projects.id','=','city_project_map.project_id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('brands','brands.id','=','projects.brand_id')
            ->whereDate('projects.expiry_date','>',Carbon::today())
            ->orderBy('city_project_map.id','desc')
            ->paginate(15);

        $data['cities'] = Cities::all();

        $data['project'] = Projects::where('is_deleted',0)->get();;
        $data['agency'] = Agency::all();
        $data['brand'] = Brands::all();


        return view('client.cities.online_cities',compact('cities','data'));
    }



    public function online_cities_search(Request $request)
    {
        $pro = DB::table('cities')
            ->select('cities.name as city_name','agencies.name as agency_name',
                'projects.name as project_name','projects.start_date','projects.expiry_date','brands.name as brand_name')
            ->leftJoin('city_project_map','city_project_map.city_id','=','cities.id')
            ->leftJoin('projects','projects.id','=','city_project_map.project_id')
            ->leftJoin('brands','brands.id','=','projects.brand_id')
            ->leftJoin('brand_project_map','brand_project_map.brand_id','=','brands.id')
            ->leftJoin('agencies','agencies.id','=','projects.agency_id')
            ->leftJoin('agency_project_map','agency_project_map.agency_id','=','agencies.id')
            ->whereDate('projects.expiry_date','>',Carbon::today())
            ->orderBy('city_project_map.id','desc');
        if($request->city){
            $pro->where('city_project_map.city_id',$request->city);
        }
        if($request->brand) {
            $pro->where('brand_project_map.brand_id', $request->brand);

        }
        if($request->agency){
            $pro->where('agency_project_map.agency_id',$request->agency);
        }
        if($request->project){
            $pro->where('projects.id',$request->project);
        }
        $cities = $pro->paginate(15);

        $data['project'] = Projects::where('is_deleted',0)->get();;
        $data['cities'] = Cities::all();
        $data['agency'] = Agency::all();
        $data['brand'] = Brands::all();


        return view('client.cities.online_cities',compact('cities','data'));
    }



}
