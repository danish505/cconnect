<?php

namespace App\Http\Controllers\Trainer;

use App\Trainers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Contracts\Auth;
use Auth;

class TrainerLoginController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('guest:trainer');
    }

    public function LoginForm()
    {
        return view('trainers.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'cnic' => 'required|min:13',
            'password' => 'required|min:6'
        ]);

        $trainer = Trainers::where('cnic', $request->cnic)->first();

        if ($trainer) {


            if (Auth::guard('trainer')->attempt(['username' => $request->cnic, 'password' => $request->password], $request->remember)) {

                return redirect()->intended(route('trainer.dashboard'));
            }

            return redirect()->back()->withInput($request->only('cnic', 'remember'));
        }

    }
}
