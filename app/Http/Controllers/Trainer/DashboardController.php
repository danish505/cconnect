<?php

namespace App\Http\Controllers\Trainer;

use App\TrainerFormData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:trainer');
    }

    public function index()
    {
        //
        $trainer = TrainerFormData::select('trainers.name','trainers.cnic','trainers_form_data.form_data',
            'projects.name as project_name','channels.name as channel_name')
            ->leftJoin('trainers','trainers.id','=','trainers_form_data.trainer_id')
            ->leftJoin('channels','channels.id','=','trainers_form_data.channel_id')
            ->leftJoin('projects','projects.id','=','trainers_form_data.project_id')
            ->leftJoin('trainer_form','trainer_form.id','=','trainers_form_data.trainer_form_id')
            ->paginate(15);



        return view('trainers.dashboard',compact('trainer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function trainer_form_data()
    {
        $data = DB::table('trainers_form_data')
            ->select('trainers_form_data.id','trainer_form.title','trainers_form_data.form_data',
                'trainer_form.form_fields','trainers_form_data.project_id as project_id','projects.name as project_name',
                'trainers_form_data.channel_id as channel_id','channels.name as channel_name',
                'trainers.name as user_name')
            ->leftJoin('trainer_form', 'trainer_form.id', '=', 'trainers_form_data.trainer_form_id')
            ->leftJoin('trainers', 'trainers.id', '=', 'trainers_form_data.trainer_id')
            ->leftJoin('channels', 'channels.id', '=', 'trainers_form_data.channel_id')
            ->leftJoin('projects', 'projects.id', '=', 'trainers_form_data.project_id')
            ->paginate(15);

        return view('trainers.manage_form_data',compact('data'));
    }

    public function trainer_form_data_view($id)
    {
        $form_data = DB::table('trainers_form_data')
            ->where('id',$id)
            ->orderBy('id','desc')
            ->get();
        $data = array();
        foreach($form_data as $key):
            $data['id'] = $key->id;
            $data['data'] = json_decode($key->form_data);
        endforeach;

        return view('trainers.form_data_view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
        auth()->logout();
        return redirect('trainer/login');
    }
}
