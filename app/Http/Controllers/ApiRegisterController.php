<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeProjectMap;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Projects;
use GuzzleHttp\Client;

class ApiRegisterController extends Controller
{
    //


    private function api_response_syntax($response, $type = "json", $response_code = 200, $outputType = JSON_PRETTY_PRINT)
    {
        $status = 1;
        $error = "";
        $returnArr = [
            "status"        => $status,
            "response"      => $response,
            "error"         => $error,
            "info"          => [
                "api_version"   => "1.0",
                "status"        => "success",
                "format"        => "json",
                "response_code" => $response_code,
                "generated_at"  => Carbon::now(),
            ]
        ];
        return response()
            ->json($returnArr, $response_code, array(), $outputType) //JSON_NUMERIC_CHECK //JSON_PRETTY_PRINT
            ->header('Access-Control-Allow-Origin','*')
            ->header('Content-Type', 'application/json;charset=utf-8');
    }


    /* ======================================
    * Function: api_error_syntax()
    * Description: returns api error syntax
    * ======================================
    * */
    private function api_error_syntax($error, $type = "json", $response_code = 200, $outputType = JSON_PRETTY_PRINT)
    {
        $status = 0;
        $response = "";
        $returnArr = [
            "status"        => $status,
            "response"      => $response,
            "error"         => $error,
            "info"          => [
                "api_version"   => "1.0",
                "status"        => "error",
                "format"        => "json",
                "response_code" => $response_code,
                "generated_at"  => Carbon::now(),
            ]
        ];
        return response()
            ->json($returnArr, $response_code, array(), $outputType)
            ->header('Access-Control-Allow-Origin','*')
            ->header('Content-Type', 'application/json;charset=utf-8');
    }

    public function show()
    {

 
    $project = DB::table('projects')->whereDate('expiry_date','>=',Carbon::today())->where('is_deleted',0)->get();

    $return_data = Array();



    foreach($project as $key):

    $brands = DB::table('brands')->select('id','name')->whereIn('id',explode(',',$key->brand_id))->get();
    $channel = DB::table('channels')->select('id','name')->whereIn('id',explode(',',$key->channel_id))->get();
    $cities = DB::table('cities')->select('id','name')->whereIn('id',explode(',',$key->cities_id))->get();
    $citiesResponse = array();
        //print_r($cities);
        //die();
        foreach($cities as $city):
            $teams = DB::table('teams_project_map')
            ->select('teams_project_map.team_id','teams.name as team_name')
            ->leftJoin('teams','teams.id','=','teams_project_map.team_id')
            ->where('teams_project_map.project_id',$key->id)
            ->where('teams_project_map.city_id',$city->id)
            ->get();

        $citynew["id"] = $city->id;
            $citynew["name"] = $city->name;
            $citynew["teams"] = $teams;

        $citiesResponse[] = $citynew;
        endforeach;

    $data['project_id'] = $key->id;
    $data['name'] = $key->name;
    $data['start_date'] = $key->start_date;
    $data['expiry_date'] = $key->expiry_date;
    $data['brands']= $brands;
    $data['channel'] = $channel;
    $data['cities'] = $citiesResponse;

    $data['status'] = $key->status;
    $return_data[] = $data;

    endforeach;
    return json_encode($return_data);

    }

        public function valid_cnic(Request $request)
        {


        if($request->cnic){
        $cnic = Employee::where('cnic',$request->cnic)->get()->toArray();

        if($cnic){

            $message = array(

                'message'=>'User exist',
                'status'=>'1'
            );
            return $message;
        }
        else
            $message = array(

                'message'=>'User is not registered',
                'status'=>'0'
            );
        return $message;

        }

        elseif ($request->cnic==false){
        $message = array(
        'message'=>'Enter Cnic',
        'status'=>'0'
        );
        return $message;

        }

        }


   public function store(Request $request)
   {


       $cnic = $request->cnic;

       $check_employee = Employee::where('cnic',$cnic)->first();

        if($check_employee){
          $error_message = array(
            'message'=>'Employee is already registered ',
            'status'=>'0'
        );
        return json_encode($error_message);
        }
        else{
/*
        inserting into employee
*/
       $employee = new Employee();

       $employee->name = $request->name;
       $employee->fathername = $request->fathername;
       $employee->cnic = $request->cnic;
       $employee->email = $request->email;
       $employee->cell = $request->cell;
       $employee->agency_email = $request->agency_email;
       $employee->status = 'pending';
       $employee->created_ip = $request->ip();
       if($request->hasFile('photo')){

           $file = $request->file('photo');
           $extension = $file->getClientOriginalExtension();
           $filename = time().'.'.$extension;
           $file->move('uploads/employee',$filename);
       }
            $employee->image = $filename;

            $employee->save();


            $employee_id = $employee->id;


       $user = new User();

       /*  inserting into users */

       $user->id = $employee_id;
       $user->username = $request->cnic;
       $user->name = $request->name;
       $user->password = Hash::make($request->password);
       $user->type = 1;
       $user->employee_id = $employee_id;
       $user->created_ip = $request->ip();
       $user->save();

       $epm = new EmployeeProjectMap();
       
        $agency = Projects::where('id',$request->project_id)->get();
        $agency_id = $agency[0]->agency_id;
        
        $epm->employee_id = $employee_id;
        $epm->project_id = $request->project_id;
        $epm->brand_id = $request->brand_id;
        $epm->city_id = $request->city_id;
        $epm->channel_id = $request->channel_id;
        $epm->team_id = $request->team_id;
         $epm->agency_id = $agency_id;
        $epm->status = 1;
        $epm->created_ip = $request->ip();

        if($epm->save()){

        $message = array(
            'message'=>'Employee is registered successfully',
            'status'=>'1'
        );
        return json_encode($message);
        }

        }
   }
    function random_num($size) {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }



   public function forget_password()
   {
       $employees = Employee::where('cnic', request('cnic'))->first();

       if (!$employees){
           return response()->json([
               'message' => 'We cant find a user with that cnic address.'], 404);
   }



        if($employees){
            $password = $this->random_num(6);

            $user = User::where('employee_id',$employees->id)->update(['password'=>Hash::make($password)]);
//

        $body =  [
            'apikey'=>'c526f7bd-3661-442a-b84c-a3d339b585fb',
            'from'=>'career.consultants.inc@gmail.com',
            'subject'=>'Forget Password',
            'to'=>'career.consultants.inc@gmail.com',
            'msgCC'=> "waqar79@gmail.com",
            'bodyHtml'=>"<!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
  <head>
    <title>
    </title>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <meta name=\"viewport\" content=\"width=device-width\">
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">
    <style type=\"text/css\">body, html {
      margin: 0px;
      padding: 0px;
      -webkit-font-smoothing: antialiased;
      text-size-adjust: none;
      width: 100% !important;
    }
      table td, table {
      }
      #outlook a {
        padding: 0px;
      }
      .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
      }
      .ExternalClass {
        width: 100%;
      }
      @media only screen and (max-width: 480px) {
        table, table tr td, table td {
          width: 100% !important;
        }
        img {
          width: inherit;
        }
        .layer_2 {
          max-width: 100% !important;
        }
        .edsocialfollowcontainer table {
          max-width: 25% !important;
        }
        .edsocialfollowcontainer table td {
          padding: 10px !important;
        }
        .edsocialfollowcontainer table {
          max-width: 25% !important;
        }
        .edsocialfollowcontainer table td {
          padding: 10px !important;
        }
      }
    </style>
  </head>
  <body style=\"padding:0; margin: 0;\">
    <table style=\"height: 100%; width: 100%; background-color: #efefef;\" align=\"center\">
      <tbody>
        <tr>
          <td valign=\"top\" id=\"dbody\" data-version=\"2.31\" style=\"width: 100%; height: 100%; padding-top: 30px; padding-bottom: 30px; background-color: #efefef;\">
            <!--[if (gte mso 9)|(IE)]><table align=\"center\" style=\"max-width:600px\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td valign=\"top\"><![endif]-->
            <table class=\"layer_1\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 600px; box-sizing: border-box; width: 100%; margin: 0px auto;\">
              <tbody>
                <tr>
                  <td class=\"drow\" valign=\"top\" align=\"center\" style=\"background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;\">
                    <!--[if (gte mso 9)|(IE)]><table width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td valign=\"top\"><![endif]-->
                    <div class=\"layer_2\" style=\"max-width: 600px; display: inline-block; vertical-align: top; width: 100%;\">
                      <table class=\"edcontent\" style=\"border-collapse: collapse;width:100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">
                        <tbody>
                          <tr>
                            <td class=\"edimg\" valign=\"top\" style=\"padding: 20px; box-sizing: border-box; text-align: center;\">
                              <img style=\"border-width: 0px; border-style: none; max-width: 160px; width: 100%;\" width=\"160\" alt=\"Image\" src=\"https://api.elasticemail.com/userfile/65296fa4-893b-4cc8-8424-b03dae20e283/1-Recovered-Recovered_03.png\">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class=\"drow\" valign=\"top\" align=\"center\" style=\"background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;\">
                    <!--[if (gte mso 9)|(IE)]><table width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td valign=\"top\"><![endif]-->
                    <div class=\"layer_2\" style=\"display: inline-block; vertical-align: top; width: 100%; max-width: 600px;\">
                      <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"edcontent\" style=\"border-collapse: collapse;width:100%\">
                        <tbody>
                          <tr>
                            <td valign=\"top\" class=\"breakline\" style=\"padding:0\">
                              <div style=\"border-style:solid none none none;border-width: 1px 0 0 0;margin-top:8px;margin-bottom:8px;\">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class=\"drow\" valign=\"top\" align=\"center\" style=\"background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;\">
                    <!--[if (gte mso 9)|(IE)]><table width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td valign=\"top\"><![endif]-->
                    <div class=\"layer_2\" style=\"display: inline-block; vertical-align: top; width: 100%; max-width: 600px;\">
                      <table border=\"0\" cellspacing=\"0\" class=\"edcontent\" style=\"border-collapse: collapse;width:100%\">
                        <tbody>
                          <tr>
                            <td valign=\"top\" class=\"edtext\" style=\"padding: 20px; text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica, Arial, sans-serif; word-break: break-word; direction: ltr; box-sizing: border-box;\">
                              <h1>Update Password 
                              </h1>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class=\"drow\" valign=\"top\" align=\"center\" style=\"background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;\">
                    <!--[if (gte mso 9)|(IE)]><table width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td valign=\"top\"><![endif]-->
                    <div class=\"layer_2\" style=\"display: inline-block; vertical-align: top; width: 100%; max-width: 600px;\">
                      <table border=\"0\" cellspacing=\"0\" class=\"edcontent\" style=\"border-collapse: collapse;width:100%\">
                        <tbody>
                          <tr>
                            <td valign=\"top\" class=\"edtext\" style=\"padding: 20px; text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica, Arial, sans-serif; word-break: break-word; direction: ltr; box-sizing: border-box;\">
                              <p style=\"margin: 0px; padding: 0px;\">We have Updated your password 
                              
                              <br/>  
                                <span>Email: $employees->email</span><br/>
                                <span>Name: $employees->name</span><br/>
                                <span>Password: $password</span><br/>
                            </p>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
" ];

        $ch = curl_init('https://api.elasticemail.com/v2/email/send');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
// execute!
            $response = curl_exec($ch);

// close the connection, release resources used

            if($response){

                $message = array(
                'message'=>'Password reset link has been sent to your email',
                'status'=>'1'
            );
                curl_close($ch);

                return json_encode($message);
            }

        }
       $message = array(
           'message'=>'This cnic or email does not exist',
           'status'=>'0'
       );
        return json_encode($message);


   }



}
