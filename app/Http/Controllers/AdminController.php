<?php

namespace App\Http\Controllers;

use App\Employee;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $users = Employee::all()->toArray();

            return view('admin.dashboard',compact('users'));
    }


    public function status_update(Request $request, $id)
    {


        $update =  Employee::find($id);



        dd($request->status);
        
        $update->status = $request->status;
        $update->save();
        return redirect('admin');



    }

    public function logout()
    {
        auth()->logout();
        return redirect('admin/login');
    }


}
