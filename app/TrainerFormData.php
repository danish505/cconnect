<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainerFormData extends Model
{
    //

    protected $table = 'trainers_form_data';
}
