<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams_project_map extends Model
{
    //
    protected $table = 'teams_project_map';



    protected $fillable = [
        'project_id', 'team_id','city_id'
    ];
}
