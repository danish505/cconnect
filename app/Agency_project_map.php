<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency_project_map extends Model
{
    //
    protected  $table = 'agency_project_map';


    protected $fillable = [
        'project_id', 'agency_id'
    ];
}
