<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel_project_map extends Model
{
    //
    protected $table = 'channel_project_map';

    protected $fillable = [
        'project_id', 'channel_id'
    ];
}
