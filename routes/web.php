<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('import-csv','CsvController@index');
Route::get('create','CsvController@store');

//Route::get('form','FormController@index');
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('find/{token}','ApiRegisterController@edit_password')->name('alter_password');
Route::post('forget/password/reset', 'PasswordResetController@reset')->name('password_reset');

Route::prefix('trainer')->group(function(){

    Route::get('/login','Trainer\TrainerLoginController@LoginForm')->name('trainer.login');
    Route::post('/login','Trainer\TrainerLoginController@login')->name('trainer.login.submit');
    Route::get('/','Trainer\DashboardController@index')->name('trainer.dashboard');
    Route::get('/form/data','Trainer\DashboardController@trainer_form_data')->name('trainer.manage.form.data');
    Route::get('/form/data/view/{id}','Trainer\DashboardController@trainer_form_data_view')->name('trainer.view.form.data');

    Route::get('/logout','Trainer\DashboardController@logout')->name('trainer.logout');


});
Route::prefix('evaluator')->group(function(){

    Route::get('/login','Evaluator\EvaluatorLoginController@LoginForm')->name('evaluator.login');
    Route::post('/login','Evaluator\EvaluatorLoginController@login')->name('evaluator.login.submit');
    Route::get('/','Evaluator\DashboardController@index')->name('evaluator.dashboard');
    Route::get('/logout','Evaluator\DashboardController@logout')->name('evaluator.logout');
    Route::get('/form/data','Evaluator\DashboardController@evaluator_form_data')->name('evaluator.manage.form.data');
    Route::get('/form/data/view/{id}','Evaluator\DashboardController@evaluator_form_data_view')->name('evaluator.view.form.data');

});

Route::prefix('client')->group(function(){

    Route::get('/login','Client\ClientLoginController@LoginForm')->name('client.login');
    Route::post('/login','Client\ClientLoginController@login')->name('client.login.submit');
    Route::get('/','Client\HomeController@index')->name('client.dashboard');
    Route::get('/logout','Client\HomeController@logout')->name('client.logout');
    Route::get('/form/data','Client\HomeController@evaluator_form_data')->name('client.manage.form.data');
    Route::get('/form/data/view/{id}','Client\HomeController@evaluator_form_data_view')->name('client.view.form.data');
    Route::get('/cities/report','Client\CitiesController@online_cities')->name('client.online.cities');
    Route::get('/cities/search/report/','Client\CitiesController@online_cities_search')->name('client.online.cities.search');
    Route::get('/users/online/report','Client\UserController@online_users')->name('client.users.online');
    Route::get('/users/online/search/report/','Client\UserController@online_users_search')->name('client.users.online.search');
    Route::get('/agency/onground/report','Client\AgencyController@onground_activation')->name('client.users.onground');
    Route::get('/agency/onground/search/report','Client\AgencyController@onground_activation_search')->name('client.users.onground.search');
    Route::get('/users/registered/report','Client\UserController@register_user')->name('client.users.register');
    Route::get('/users/registered/search/report','Client\UserController@register_user_search')->name('client.register.user.search');
    Route::get('/users/break/report','Client\UserController@users_break')->name('client.users.break');
    Route::get('/users/break/search/report/','Client\UserController@users_break_search')->name('client.users.break.search');
    Route::get('employee/data/report','Client\FormController@form_data_report')->name('client.employee.data.report');
    Route::get('employee/data/report/search','Client\FormController@form_data_report_search')->name('client.employee.data.report.search');
    Route::get('form/data/','Client\FormController@form_get')->name('client.form.data');
    Route::post('form/data/','Client\FormController@form_get_search')->name('client.form.data_post');
    Route::get('form/data/{id}','Client\FormController@form_data_view');

});



Route::prefix('admin')->group(function()
{
    Route::get('/login', 'Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/','Admin\AdminController@index')->name('admin.dashboard');


    /*
     *  Users Data
     *
     * */

    Route::get('/users/','Admin\UserController@index')->name('admin.users');
    Route::get('/user/profile/{id}','Admin\UserController@user_profile');
    Route::get('/users/channels','Admin\UserController@manage_channels')->name('admin.users.channels');
    Route::post('/users/channels/search','Admin\UserController@user_search_channels')->name('admin.user.channel.search');
    Route::post('/users/search','Admin\UserController@user_search')->name('admin.user.search');
    Route::post('/users/update/status/{id}','Admin\UserController@status_update');
    Route::get('/users/channels/edit/{id}','Admin\UserController@edit');
    Route::get('/users/online/report','Admin\UserController@online_users')->name('admin.users.online');
    Route::get('/users/online/search/report/','Admin\UserController@online_users_search')->name('admin.users.online.search');
    Route::get('/users/registered/report','Admin\UserController@register_user')->name('admin.users.register');
    Route::get('/users/registered/search/report','Admin\UserController@register_user_search')->name('admin.register.user.search');
    Route::get('/users/break/report','Admin\UserController@users_break')->name('admin.users.break');
    Route::get('/users/break/search/report/','Admin\UserController@users_break_search')->name('admin.users.break.search');
    Route::post('/users/channels/update/{id}','Admin\UserController@update');


    /*
    *  Agency CRUD
    */
    Route::get('agency','Admin\AgencyController@index')->name('admin.agency');
    Route::get('/agency/onground/report','Admin\AgencyController@onground_activation')->name('admin.users.onground');
    Route::get('/agency/onground/search/report','Admin\AgencyController@onground_activation_search')->name('admin.users.onground.search');

    Route::get('agency/create','Admin\AgencyController@create')->name('admin.agency.create');
    Route::post('agency/create','Admin\AgencyController@store')->name('admin.agency.create');
    Route::get('agency/edit/{id}','Admin\AgencyController@edit');
    Route::post('agency/edit/{id}','Admin\AgencyController@update');
    Route::get('agency/delete/{id}','Admin\AgencyController@destroy');
    /*
     *  cities
      */
    Route::get('/cities/','Admin\CitiesController@index')->name('admin.cities');
    Route::get('/cities/report','Admin\CitiesController@online_cities')->name('admin.online.cities');
    Route::get('/cities/search/report/','Admin\CitiesController@online_cities_search')->name('admin.online.cities.search');
    Route::get('/cities/create/','Admin\CitiesController@create')->name('admin.cities.create');
    Route::post('/cities/create/','Admin\CitiesController@store')->name('admin.cities.insert');
    Route::get('/cities/edit/{id}','Admin\CitiesController@edit');
    Route::post('/cities/edit/{id}','Admin\CitiesController@update');
    Route::get('/cities/delete/{id}','Admin\CitiesController@destroy')->name('admin.cities.delete');


    /*
     * CRUD Brands
     *
     * */
    Route::get('/brands/','Admin\BrandController@index')->name('admin.brands');
    Route::get('/brands/create','Admin\BrandController@create')->name('admin.brands.create');
    Route::post('/brands/create','Admin\BrandController@store');
    Route::get('/brands/edit/{id}','Admin\BrandController@edit');
    Route::post('/brands/update/{id}','Admin\BrandController@update');
    Route::get('/brands/delete/{id}','Admin\BrandController@destroy');



    /*
     * CRUD Teams
     *
     *
     * */

    Route::get('/teams/','Admin\TeamController@index')->name('admin.teams');
    Route::get('/teams/create','Admin\TeamController@create')->name('admin.team.create');
    Route::post('/teams/create','Admin\TeamController@store');
    Route::get('/teams/edit/{id}','Admin\TeamController@edit');
    Route::post('/teams/update/{id}','Admin\TeamController@update');
    Route::get('/teams/delete/{id}','Admin\TeamController@destroy');


    /*
     *  Form
    */

    Route::get('/forms/','Admin\FormController@index')->name('admin.forms');
    Route::get('/forms/project/map/','Admin\FormController@project_form_map')->name('admin.project.form.view');
    Route::post('/forms/project/map/callback','Admin\FormController@project_form_map_callback');
    Route::get('/forms/project/map/create','Admin\FormController@create_project_form_map')->name('admin.project.form.create');
    Route::get('/forms/project/map/edit/{id}','Admin\FormController@edit_project_form_map');
    Route::post('/forms/project/map/create','Admin\FormController@store_project_form_map')->name('admin.project.form.map');
    Route::get('/form/view/{id}','Admin\FormController@show')->name('admin.view_forms');
    Route::get('/form/edit/{id}','Admin\FormController@edit');
    Route::post('/form/update/status/{id}','Admin\FormController@update_status');
    Route::post('/form/update/{id}','Admin\FormController@update');
    Route::get('form/create','Admin\FormController@create')->name('admin.form.create');
    Route::post('form/create','Admin\FormController@store');
    Route::get('form/delete/{id}','Admin\FormController@destroy');
    Route::get('form/data/','Admin\FormController@form_get')->name('admin.form.data');
    Route::post('form/data/','Admin\FormController@form_get_search')->name('admin.form.data_post');
    Route::get('employee/data/report','Admin\FormController@form_data_report')->name('admin.employee.data.report');
    Route::get('employee/data/report/search','Admin\FormController@form_data_report_search')->name('admin.employee.data.report.search');
    Route::get('form/data/{id}','Admin\FormController@form_data_view');
    Route::get('form/filters/','Admin\FormController@form_data_filters');
    Route::post('forms/project/channel/callback','Admin\FormController@project_channel_form_map_callback');

    Route::post('form/data/search','Admin\FormController@form_data_search')->name('admin.forms.data.search');

    Route::get('/new',function(){
       return view('admin.projects.newproject');
    });


    /*
     * Channels
     *
     * */

    Route::get('/channels','Admin\ChannelController@index')->name('admin.channels');
    Route::get('/channels/form/create/{id}','Admin\ChannelController@create_form_template');
    Route::get('/channels/form/edit/{id}','Admin\ChannelController@edit_form_template');
    Route::post('/channels/form/update/{id}','Admin\ChannelController@update_form_channel');
    Route::post('/channels/form/store','Admin\ChannelController@store_form_template');
    Route::get('/channels/create/','Admin\ChannelController@create')->name('admin.channels.create');
    Route::post('/channels/create/','Admin\ChannelController@store');
    Route::get('/channels/edit/{id}','Admin\ChannelController@edit');
    Route::post('/channels/update/{id}','Admin\ChannelController@update');
    Route::get('/channels/delete/{id}','Admin\ChannelController@destroy');

    /*
     *
     * PROJECTS 
     *
     * */

    Route::get('/projects','Admin\ProjectController@index')->name('admin.projects');
    Route::get('/projects/form/map/edit/{id}','Admin\ProjectController@edit_form_map_project_id');
    Route::get('/projects/channel/form/map/{project_id}/{id}','Admin\ProjectController@create_or_edit_form');
    Route::post('/projects/channel/form/create/','Admin\ProjectController@project_form_store');
    Route::post('/projects/channel/form/update/{id}','Admin\ProjectController@update_project_form');

    Route::get('/projects/create','Admin\ProjectController@create')->name('admin.projects.create');
    Route::post('/projects/create','Admin\ProjectController@store')->name('admin.projects.store');
    Route::get('/projects/edit/{id}','Admin\ProjectController@edit');
    Route::get('/projects/status/','Admin\ProjectController@project_user_status')->name('admin.project.status');
    Route::post('/projects/status/update/{id}','Admin\ProjectController@project_user_status_update');
    Route::post('/projects/edit/{id}','Admin\ProjectController@update');
    Route::get('/projects/delete/{id}','Admin\ProjectController@destroy');


    /*
     *
     * TRAINERS
     *
     */
    Route::get('/trainers/create','Admin\TrainerController@create')->name('admin.trainer.create');
    Route::post('/trainers/create','Admin\TrainerController@store')->name('admin.trainer.store');
    Route::get('/trainers/','Admin\TrainerController@index')->name('admin.trainer.manage');
    Route::get('/trainers/','Admin\TrainerController@index')->name('admin.trainer.manage');

    /**
     *
     * Evaluators
     */
    Route::get('/evaluator/create','Admin\EvaluatorController@create')->name('admin.evaluator.create');
    Route::post('/evaluator/store','Admin\EvaluatorController@store')->name('admin.evaluator.store');
    Route::get('/evaluator/','Admin\EvaluatorController@index')->name('admin.evaluator.manage');


    /*
     *  LOGOUT
    */
    Route::get('/logout','Admin\AdminController@logout')->name('admin.logout');
});