<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([

    'middleware' => 'api',
    'prefix' => 'v1'

], function () {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('projects','ApiRegisterController@show');
    Route::post('valid/cnic','ApiRegisterController@valid_cnic');
    Route::post('signup/employee','ApiRegisterController@store');
    Route::post('user/clockin','ApiController@clock_in');
    Route::post('user/check/breakin','ApiController@check_breakin');
    Route::post('user/check/breakout','ApiController@check_breakout');
    Route::post('user/check/clockin','ApiController@check_clockin');
    Route::post('user/check/clockout','ApiController@check_clockout');
    Route::post('user/clockout','ApiController@clock_out');
    Route::post('user/breakin','ApiController@break_in');
    Route::post('user/breakout','ApiController@break_out');
    Route::post('form','ApiController@form_show');
    Route::post('forget/password','ApiRegisterController@forget_password');
    Route::post('form/data','ApiController@form_data_receive');
    Route::post('employee/project/map','ApiController@store_emp_project_map');
    Route::post('evaluator/form','ApiController@evaluator_form');
    Route::post('trainer/form','ApiController@trainer_form');
    Route::post('trainer/data','ApiController@trainer_form_get_data');
    Route::post('evaluator/data','ApiController@evaluator_form_get_data');

});

