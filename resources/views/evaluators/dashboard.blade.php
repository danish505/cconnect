@extends('layouts.evaluator-master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Form Data
                            {{--                        <a href="{{ route('admin.brands.create') }}" class="btn btn-primary float-right">Add Brands</a>--}}
                        </h3>
                        <br/>

                    </div>
                </div>


                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Evaluator Name</th>
                                    <th>Cnic</th>
                                    <th>Project</th>
                                    <th>Channel</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($evaluator as $eval)
                                    <?php $id++;?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $eval->name }}</td>
                                        <td>{{ $eval->cnic }}</td>
                                        <td>{{ $eval->project_name }}</td>
                                        <td>{{ $eval->channel_name }}</td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{ $evaluator->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection