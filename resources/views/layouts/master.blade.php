<!DOCTYPE html>


<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>C Connect | Dashboard</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->
    <!--begin::Global Theme Styles -->
    <link href="{{asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/general/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{ asset('assets/demo/demo3/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/demo3/base/custom.css') }}" rel="stylesheet" type="text/css" />

{{--    <link href="{{ asset('vendors/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ asset('vendors/owl.carousel/dist/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ asset('vendors/owl.carousel/dist/assets/owl.theme.default.css') }}" rel="stylesheet" type="text/css" />--}}
<!--RTL version:<link href="assets/demo/demo3/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Global Theme Styles -->
    <!--begin:: Global Optional Vendors -->

    <!--End:: Global Optional Vendors -->

    <link href="{{ asset('vendors/vendors/metronic/css/styles.css') }}" rel="stylesheet" type="text/css" />


    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-stack__item--center m-brand__logo">
                            <a href="#" class="m-brand__logo-wrapper pt-3">
                                <img alt="" src="{{ asset('img/logo-dashboard.png') }}" />
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>

                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>

                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>


                    <!-- END: Horizontal Menu -->

                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="
	m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light" m-dropdown-toggle="click" m-dropdown-persistent="1"
                                    id="m_quicksearch" m-quicksearch-mode="dropdown">
                                    <!-- <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-nav__link-icon"><i class="flaticon-search-1"></i></span>
                                    </a> -->
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                            <div class="m-dropdown__inner ">
                                <div class="m-dropdown__header">
                                    <form class="m-list-search__fo  rm">
                                        <div class="m-list-search__form-wrapper">
                                                    <span class="m-list-search__form-input-wrapper">
                                                        <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
                                                    </span>
                                            <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                                                        <i class="la la-remove"></i>
                                                    </span>
                                        </div>
                                    </form>
                                </div>
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                                        <div class="m-dropdown__content">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                </li>

                    <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                        m-dropdown-toggle="click">
                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-topbar__userpic">
                             <img src="{{asset('assets/app/media/img/users/user2.jpg')}}" alt="" />
                                    </span>
                        </a>
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__header m--align-center" style="background: url('{{ asset('assets/app/media/img/misc/user_profile_bg.jpg') }}'); background-size: cover;">
                                    <div class="m-card-user m-card-user--skin-dark">
                                        <div class="m-card-user__pic">
                                            <img src="{{ asset('assets/app/media/img/users/user2.jpg') }}" alt="" />
                                        </div>
                                        <div class="m-card-user__details">
                                            <span class="m-card-user__name m--font-weight-500">{{ auth()->user()->name }}</span>
                                            <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ auth()->user()->email }}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav m-nav--skin-light">

                                            <li class="m-nav__separator m-nav__separator--fit">
                                            </li>
                                            <li class="m-nav__item">
                                                <a href="{{ route('admin.logout')}}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                            </ul>
                        </div>
                    </div>

                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>

    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn"><i class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown ">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow">
                    <li class="m-menu__item  m-menu__item--active"><a href="{{ route('admin.dashboard') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Dashboard</span></a></li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-list-3"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Users Reports</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.users.online') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Online Users</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.users.onground') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Activation Onground</span></a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.users.break') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Users On Break</span></a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.employee.data.report') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Intercepted Customers Data</span></a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.online.cities') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Online Cities</span></a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.users.register') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Registered Users</span></a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.form.data') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Submitted Form Data</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('admin/form/filters/') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">View Form Data</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-users"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Users</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.users.channels') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Users Channels</span></a></li>
{{--                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.users') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Users Permission</span></a></li>--}}
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.project.status') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Project Approval</span></a></li>

                            </ul>
                        </div>
                    </li>



                    {{--                    <li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="{{ route('admin.users.channels') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-list-2"></i><span class="m-menu__link-text">Manage Users</span></a></li>--}}
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-list-2"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Projects</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.projects') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Projects</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.projects.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Projects</span></a></li>

                            </ul>
                        </div>
                    </li>


                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-list-1"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Forms</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.forms') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">View Forms</span></a></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.form.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Form</span></a></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.project.form.view') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Project Form Mapping</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.project.form.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text"> Create Project Form Mapping</span></a></li>


                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-users"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Cities</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.cities') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Cities </span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.cities.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create City</span></a></li>

                            </ul>
                        </div>
                    </li>



                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-imac"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Channels</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.channels') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Channels </span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.channels.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Channels</span></a></li>

                            </ul>
                        </div>
                    </li>


                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-technology-1"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Brands</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.brands') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Brands </span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.brands.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Brands</span></a></li>
                             </ul>
                        </div>
                    </li>



                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-map"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Teams</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.teams') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Teams</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.team.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Teams</span></a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-share"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Agency</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.agency') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Agency</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.agency.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Agency</span></a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-share"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Trainers</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.trainer.manage') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Trainer</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.trainer.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Trainer</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-share"></i>
                            <span class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">Evaluator</span>
                                </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Reports</span>
                                <span class="m-menu__link-badge"> </span> </span></span></span></li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ route('admin.evaluator.manage') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Manage Evaluator</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('admin.evaluator.create') }}" class="m-menu__link "><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Create Evaluator</span></a></li>
                            </ul>
                        </div>
                    </li>





                </ul>
            </div>

            <!-- END: Aside Menu -->
        </div>

        <!-- END: Left Aside -->
        @yield('content')
    </div>

    <!-- end:: Body -->

    <!-- begin::Footer -->
{{--    <footer class="m-grid__item	m-footer ">--}}
{{--        <div class="m-container m-container--fluid m-container--full-height m-page__container">--}}
{{--            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">--}}
{{--                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">--}}
{{--							<span class="m-footer__copyright">--}}
{{--								2019 &copy; CCI Connect Powered By <a href="#" class="m-link">Minhasoft</a>--}}
{{--							</span>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </footer>--}}

<!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Quick Sidebar -->


<!-- end::Quick Sidebar -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<!-- end::Scroll Top -->

<!-- begin::Quick Nav -->


<!-- begin::Quick Nav -->

<!--begin::Global Theme Bundle -->


<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/demo3/base/scripts.bundle.js') }}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->





<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<!--end::Page Vendors -->

<!--begin::Page Scripts -->

<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@yield('form-data-script')

@yield('create-project-script')

<script src="{{ asset('assets/demo/demo3/base/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/app/bundle/app.bundle.js') }}" type="text/javascript"></script>



<script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>


<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>