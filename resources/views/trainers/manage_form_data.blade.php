@extends('layouts.trainer-master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Trainer Form Data
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">

                    <!--begin: Search Form -->
{{--                    <form class="m-form m-form--fit m--margin-bottom-20" method="post" action="{{ route('admin.form.data_post') }}">--}}
{{--                        @csrf--}}
{{--                        <div class="row m--margin-bottom-20">--}}
{{--                            <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">--}}
{{--                                <label>Project:</label>--}}
{{--                                <select class="form-control m-select2" id="kt_select2_3" name="project">--}}
{{--                                    <option selected>Please select</option>--}}
{{--                                    @foreach($data['project'] as $project)--}}
{{--                                        <option value="{{ $project->id }}">{{ $project->name }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">--}}
{{--                                <label>Channel:</label>--}}
{{--                                <select class="form-control m-select2" id="kt_select2_4" name="channel">--}}
{{--                                    <option selected>Please select</option>--}}
{{--                                    @foreach($data['channels'] as $channel)--}}
{{--                                        <option value="{{ $channel->id }}">{{ $channel->name }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}


{{--                            <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">--}}
{{--                                <label>City:</label>--}}
{{--                                <select class="form-control m-select2" id="kt_select2_5" name="city">--}}
{{--                                    <option selected>Please select</option>--}}
{{--                                    @foreach($data['cities'] as $city)--}}
{{--                                        <option value="{{ $city->id }}">{{ $city->name }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row m--margin-bottom-20">--}}
{{--                        </div>--}}
{{--                        <div class="m-separator m-separator--md m-separator--dashed"></div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12">--}}
{{--                                <button class="btn btn-primary m-btn m-btn--icon" id="m_search">--}}
{{--                            <span>--}}
{{--                                <i class="la la-search"></i>--}}
{{--                                <span>Search</span>--}}
{{--                            </span>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}

                    <!--begin: Datatable -->
                    <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 895px;">
                                    <thead>
                                    <tr role="row">
                                        <th rowspan="1" colspan="1">
                                            S.No
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Title
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Project
                                        </th>

                                        <th rowspan="1" colspan="1">
                                            Channels
                                        </th>

                                        <th rowspan="1" colspan="1">
                                            User
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Action
                                        </th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr role="row">
                                        <th rowspan="1" colspan="1">
                                            S.No
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Title
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Project
                                        </th>

                                        <th rowspan="1" colspan="1">
                                            Channels
                                        </th>

                                        <th rowspan="1" colspan="1">
                                            User
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Action
                                        </th>

                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $id = 0; ?>
                                    @if(isset($data))
                                        @foreach($data as $key)
                                            <?php  $id++; ?>
                                            <tr role="row" class="odd">


                                                <td class="sorting_1" tabindex="0">
                                                    {{ $id }}
                                                </td>

                                                <td rowspan="1" colspan="1">
                                                    {{ $key->title}}
                                                </td>
                                                <td rowspan="1" colspan="1">
                                                    {{ $key->project_name }}
                                                </td>

                                                <td rowspan="1" colspan="1">
                                                    {{ $key->channel_name }}
                                                </td>


                                                <td rowspan="1" colspan="1">
                                                    {{ $key->user_name}}
                                                </td>
                                                <td rowspan="1" colspan="1">
                                                    <a href="{{ url('trainer/form/data/view/') }}/<?= $key->id;?>" class="btn btn-primary">View</a>
                                                </td>

                                            </tr>
                                        @endforeach
                                        {{ $data->links() }}
                                    @endif




                                    </tbody>
                                </table>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('form-data-script')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    {{--    <script src="{{ asset('assets/demo/default/custom/crud/datatables/search-options/advanced-search.js') }}" type="text/javascript"></script>--}}

@endsection
