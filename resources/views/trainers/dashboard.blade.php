@extends('layouts.trainer-master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="kt-portlet mng-users-table">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Manage Form Data
{{--                        <a href="{{ route('admin.brands.create') }}" class="btn btn-primary float-right">Add Brands</a>--}}
                    </h3>
                    <br/>

                </div>
            </div>


            <div class="kt-portlet__body">
                <!--begin::Section-->
                <div class="kt-section">
                    <div class="kt-section__content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Trainer Name</th>
                                <th>Cnic</th>
                                <th>Project</th>
                                <th>Channel</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $id = 0; ?>
                            @foreach($trainer as $train)
                                <?php $id++;?>
                                <tr>
                                    <th scope="row">{{ $id }}</th>
                                    <td>{{ $train->name }}</td>
                                    <td>{{ $train->cnic }}</td>
                                    <td>{{ $train->project_name }}</td>
                                    <td>{{ $train->channel_name }}</td>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    {{ $trainer->links() }}

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection