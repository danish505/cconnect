@extends('layouts.client-master')

@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           View Form Data
{{--                            <a href="{{ url('admin/form/create') }}" class="btn btn-primary float-right ">Create Form</a>--}}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Label</th>
                                    <th>User Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($data['data'] as $form)
                                    <?php $id ++;
                                    ?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td><?php if(isset($form->label)){ echo $form->label; } ?></td>
                                        <td><?php if(isset($form->userValue)){ echo $form->userValue; } ?> </td>


                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection