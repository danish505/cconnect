
    @extends('layouts.master')

@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Forms
                            {{--<a href="{{ url('admin/form/create') }}" class="btn btn-primary float-right ">Create Form</a>--}}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Form Title</th>
                                    <th>Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($forms as $form)
                                    <?php $id ++; ?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $form->title }}</td>
                                         <td>

                                         </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


