@extends('layouts.client-master')
@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Online Cities
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <!--begin: Search Form -->
                    <form class="m-form m-form--fit m--margin-bottom-20" method="get" action="{{ route('client.online.cities.search') }}">
                        @csrf
                        <div class="row m--margin-bottom-20">
                            <div class="col-lg-6 m--margin-bottom-10-tablet-and-mobile">
                                <label>Activation Partner:</label>

                                <select class="form-control m-select2" id="kt_select2_1" name="agency">
                                    <option>Please Select</option>
                                    @foreach($data['agency'] as $agency)
                                        <option value="{{ $agency->id }}">{{ $agency->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 m--margin-bottom-10-tablet-and-mobile">
                                <label>Brand:</label>
                                <select class="form-control m-select2" id="kt_select2_2" name="brand">
                                    <option>Please Select</option>
                                    @foreach($data['brand'] as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row m--margin-bottom-20">
                            <div class="col-lg-6 m--margin-bottom-10-tablet-and-mobile">
                                <label>Project:</label>
                                <select class="form-control m-select2" id="kt_select2_3" name="project">
                                    <option>Please Select</option>
                                    @foreach($data['project'] as $project)
                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-lg-6 m--margin-bottom-10-tablet-and-mobile">
                                <label>City:</label>
                                <select class="form-control m-select2" id="kt_select2_4" name="city">
                                    <option>Please Select</option>
                                    @foreach($data['cities'] as $key)
                                        <option value="{{ $key->id }}">{{ $key->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row m--margin-bottom-20">
                        </div>
                        <div class="m-separator m-separator--md m-separator--dashed"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-primary m-btn m-btn--icon" id="m_search">
                                    <span>
                                        <i class="la la-search"></i>
                                        <span>Search</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <!--begin: Datatable -->
                    <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12"><table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 895px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" aria-sort="ascending">
                                            S.NO
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Ship City: activate to sort column ascending">
                                            City
                                        </th>

                                        <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Country: activate to sort column ascending">
                                            Activation Partner
                                        </th>

                                        <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Company Agent: activate to sort column ascending">
                                            Brand
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Status: activate to sort column ascending">
                                            Project
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Status: activate to sort column ascending">
                                            Start date
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Status: activate to sort column ascending">
                                            End date
                                        </th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">S.NO</th>
                                        <th rowspan="1" colspan="1">City</th>
                                        <th rowspan="1" colspan="1">Activation Partner</th>
                                        <th rowspan="1" colspan="1">Brand</th>
                                        <th rowspan="1" colspan="1">Project</th>
                                        <th rowspan="1" colspan="1">Start Date</th>
                                        <th rowspan="1" colspan="1">End Date</th>


                                    </tr>
                                    </tfoot>
                                    <tbody><?php $id = 0; ?>
                                    @foreach($cities as $key)
                                        <?php $id++ ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1" tabindex="0">{{ $id }}</td>
                                            <td>{{ $key->city_name }}</td>
                                            <td>{{ $key->agency_name }}</td>
                                            <td>{{ $key->brand_name }}</td>
                                            <td>{{ $key->project_name }}</td>
                                            <td>{{ $key->start_date }}</td>
                                            <td>{{ $key->expiry_date }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $cities->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection