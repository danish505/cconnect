<html>
<head>
<title>Form</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<div id="build-wrap">

</div>

<button id="saveData">Save</button>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="http://formbuilder.online/assets/js/form-builder.min.js"></script>


<script>

    //  document.getElementById('showData').addEventListener('click', function() {
    //   var formData =  formBuilder.actions.getData('json',true);
    //             console.log(formData);
    //     var data = $('#data').val();
    //     data = formData;
    //       data_array = [formData];
    //      $.ajax({
    //          headers: {
    //              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //          },
    //          url:"http://localhost:8000/formsubmit",
    //          method:"POST",
    //          data: JSON.stringify(data_array)
    //      })
    // });

    // document.getElementById("saveData").addEventListener("click", () => formBuilder.actions.save());
    var fbEditor = $('#build-wrap').formBuilder();



    // Post the data to a script which will save it to your db
    $( "#saveData" ).on( 'click', function(){

        $.ajax({
            headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        // 'Content-Type': 'application/json'
                        'Access-Control-Allow-Origin':'*'
                     },
            url: 'http://localhost:8000/formsubmit', // Point this to the file you create to save data
            type: 'POST',
            data: {
                formStructure: JSON.stringify( fbEditor.actions.getData('json') )
            },
            success: function( response ){

                // Got a response
                console.log( 'Response from the save action: ' + response );

            },
            error: function( jqXHR ){

                // Something went wrong
                console.log( 'Error saving the form (details below)' );
                console.log( jqXHR );

            }

        });

    });

</script>


</body>

</html>