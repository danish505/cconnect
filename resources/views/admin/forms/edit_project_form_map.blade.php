@extends('layouts.master')


@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Form Mapping
                        </h3>
                    </div>
                </div>




                <form method="post" action="{{ route('admin.project.form.map') }}">
                    @csrf
                    <div class="row">

                        <div class="col-md-4">

                            <div class="form-group m-form__group pt-5">
                                <label>Projects</label>
                                <select class="form-control" id="project" name="project">
                                    <option>Please Select</option>
                                    @foreach($data['projects'] as $pro)

                                        <option value="{{ $pro->id }}">{{ $pro->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                            <div class="col-md-4 pt-5">
                                <label>Channels</label>

                                <select id="channel" class="form-control" name="channel">

                                </select>
                            </div>

                        <div class="col-md-4 pt-5">
                            <label>Forms</label>

                            <select id="forms" class="form-control" name="form">
                                @foreach($data['forms'] as $form)
                                    <option value="{{ $form->id }}">{{ $form->title }}</option>
                                @endforeach
                            </select>
                        </div>




                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-5">
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>


            </div>


        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>


$('#project').change(function(){

      data = $('#project').val();
      $('#channel').empty();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                // 'Content-Type': 'application/json'
                'Access-Control-Allow-Origin':'*'
            },
            url: "{{ url('admin/forms/project/map/callback') }}", // Point this to the file you create to save data
            type: 'POST',
            data:{id:data},
            success: function( response ){


                // Got a response
                cdata = JSON.parse(response);

                // console.log(cdata);
                for(i=0; i<=cdata.length; i++)
                {
                 $('#channel').append("<option value="+cdata[i].id+">"+ cdata[i].name+"</option>");

                }

        },
            error: function( jqXHR ){

                // Something went wrong
                console.log( 'Error saving the form (details below)' );
                console.log( jqXHR );

            }

        });

});
</script>
@endsection
