@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">Employee Data Report</h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <span class="m-nav__link-text">Forms &amp; Controls</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <span class="m-nav__link-text">Form Layouts</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <span class="m-nav__link-text">Multi Column Forms</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>

        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Employee Data Report
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">

                    <!--begin: Search Form -->
                    <form class="m-form m-form--fit m--margin-bottom-20" method="post" action="{{ route('admin.forms.data.search') }}">
                        @csrf
                        <div class="row m--margin-bottom-20">
                            <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>Project:</label>
                                <input type="text" class="form-control m-input" name="project" placeholder="Project" data-col-index="0">
                            </div>
                            <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>City:</label>
                                <input type="text" class="form-control m-input" name="city" placeholder="City" data-col-index="1">
                            </div>
                            <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>Channel:</label>
                                <input type="text" class="form-control m-input" name="channel" placeholder="City" data-col-index="1">
                            </div>
                            <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>Date:</label>
                                <div class="input-daterange input-group" id="m_datepicker">
                                    <input type="text" class="form-control m-input" name="start" placeholder="From" data-col-index="5">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                    </div>
                                    <input type="text" class="form-control m-input" name="end" placeholder="To" data-col-index="5">
                                </div>
                            </div>

                        </div>
                        <div class="row m--margin-bottom-20">

                        </div>
                        <div class="m-separator m-separator--md m-separator--dashed"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-brand m-btn m-btn--icon" id="m_search">
												<span>
													<i class="la la-search"></i>
													<span>Search</span>
												</span>
                                </button>
                                &nbsp;&nbsp;
                                <button class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
												<span>
													<i class="la la-close"></i>
													<span>Reset</span>
												</span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <!--begin: Datatable -->
                    <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 895px;">
                                    <thead>
                                    <tr role="row">
                                        <th rowspan="1" colspan="1">
                                            S.No
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Title
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Project
                                        </th>

                                        <th rowspan="1" colspan="1">
                                            Channels
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            City
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Action
                                        </th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr role="row">
                                        <th rowspan="1" colspan="1">
                                            S.No
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Title
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Project
                                        </th>

                                        <th rowspan="1" colspan="1">
                                            Channels
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            City
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            Action
                                        </th>

                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $id = 0; ?>
                                    @if(isset($search))
                                        @foreach($search as $key)
                                            <?php  $id++; ?>
                                            <tr role="row" class="odd">


                                                <td class="sorting_1" tabindex="0">
                                                    {{ $id }}
                                                </td>

                                                <td rowspan="1" colspan="1">
                                                    {{ $key->title}}
                                                </td>
                                                <td rowspan="1" colspan="1">
                                                    {{ $key->project_name }}
                                                </td>

                                                <td rowspan="1" colspan="1">
                                                    {{ $key->channel_name }}
                                                </td>
                                                <td rowspan="1" colspan="1">
                                                    {{ $key->city_name }}
                                                </td>
                                                <td rowspan="1" colspan="1">
                                                    <a href="{{ url('admin/form/data') }}/<?= $key->id;?>" class="btn btn-primary">View</a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif




                                    </tbody>
                                </table>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('form-data-script')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
{{--    <script src="{{ asset('assets/demo/default/custom/crud/datatables/search-options/advanced-search.js') }}" type="text/javascript"></script>--}}

@endsection
