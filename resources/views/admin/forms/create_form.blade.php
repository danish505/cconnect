<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>CCI Connect | Create Form</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('assets/demo/demo3/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/demo3/base/custom.css') }}" rel="stylesheet" type="text/css" />

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->

    <!--begin::Global Theme Styles -->
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--RTL version:<link href="assets/demo/demo3/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Global Theme Styles -->

    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="{{ asset('assets/demo/demo3/media/img/logo/logo.png') }}" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark " style="background:white">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-stack__item--center m-brand__logo">
                            <a href="{{ route('admin.dashboard') }}" class="m-brand__logo-wrapper">
                                <img alt="" src="{{ asset('img/logo-dashboard.png') }}" />
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>

                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>

                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                    <a href="{{ route('admin.dashboard') }}" class="back-btn btn-primary btn"><span class="back-arrow"><</span> Back</a>
                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>


                    <!-- END: Horizontal Menu -->

                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-dropdown__wrapper m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light" m-dropdown-toggle="click" m-dropdown-persistent="1"
                                    id="m_quicksearch" m-quicksearch-mode="dropdown"><!-- <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-nav__link-icon"><i class="flaticon-search-1"></i></span>
                                    </a> -->
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner ">
                                            <div class="m-dropdown__header">
                                                <form class="m-list-search__form">
                                                    <div class="m-list-search__form-wrapper">
																<span class="m-list-search__form-input-wrapper">
																	<input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
																</span>
                                                        <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
																	<i class="la la-remove"></i>
																</span>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                                                    <div class="m-dropdown__content">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="m-nav__item m-topbar__user-profile popup m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    onclick="myFunction()">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="{{ asset('assets/app/media/img/users/user3.jpg') }}" alt="" />
												</span>
                                    </a>
                                    <div class="popuptext custom-popup" id="myPopup">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner custom-drop-down">
                                            <div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-dark">
                                                    <div class="m-card-user__pic">
                                                        <img src="{{ asset('assets/app/media/img/users/user3.jpg') }}" alt="" />
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">{{ auth()->user()->name }}</span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ auth()->user()->email }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">

                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ route('admin.logout') }}" class="btn m-btn--pill custom-logout btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>

    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">



        <!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->


            <!-- END: Subheader -->
<div class="m-content builder-form-content">
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 form-logo padding-0">
                <!-- <img src="assets/demo/demo3/media/img/logo/logo-large.png"> -->
                <div class="title-form col-sm-12">
                    <label>Title</label>
                    <input type="text" name="title" id="title" class="form-control"  required>
                </div>
            </div>
        </div>
    </div>
</section>
                <!-- BEGIN: Subheader -->
                <!-- END: Subheader -->
<section>
    <div class="container form-builder-container">
        <!-- <h3 class="m-subheader__title ">Create Form</h3> -->
        <div id="build-wrap" class="form-builder-custom">
        </div>
        <button id="saveData" class="builder-save-btn btn-primary">Save</button>
    </div>
</section>
</div>
    <!-- Type a message here -->
</div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="http://formbuilder.online/assets/js/form-builder.min.js"></script>

    <script>

        //  document.getElementById('showData').addEventListener('click', function() {
        //   var formData =  formBuilder.actions.getData('json',true);
        //             console.log(formData);
        //     var data = $('#data').val();
        //     data = formData;
        //       data_array = [formData];
        //      $.ajax({
        //          headers: {
        //              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //          },
        //          url:"http://localhost:8000/formsubmit",
        //          method:"POST",
        //          data: JSON.stringify(data_array)
        //      })
        // });

        // document.getElementById("saveData").addEventListener("click", () => formBuilder.actions.save());
        var fbEditor = $("#build-wrap").formBuilder();



        // Post the data to a script which will save it to your db
        $( "#saveData" ).on( 'click', function(){
            var title = $('#title').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    // 'Content-Type': 'application/json'
                    'Access-Control-Allow-Origin':'*'
                },
                url: "{{ url('admin/form/create') }}", // Point this to the file you create to save data
                type: 'POST',
                data: {
                    formStructure: JSON.stringify( fbEditor.actions.getData('json') ),
                    title:title
                },
                success: function( response ){

                    // Got a response
                    console.log( 'Response from the save action: ' + response );
                    window.location.href ='{{ url('admin/forms') }}';

                },
                error: function( jqXHR ){

                    // Something went wrong
                    console.log( 'Error saving the form (details below)' );
                    console.log( jqXHR );

                }

            });

        });
        function myFunction() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");
        }

    </script>


<!-- end:: Body -->

<!-- end::Footer -->


<!-- end:: Page -->

<!-- begin::Quick Sidebar -->


<!-- begin::Quick Nav -->

<!--begin::Global Theme Bundle -->
<!-- <script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="assets/demo/demo3/base/scripts.bundle.js" type="text/javascript"></script> -->

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors -->
<!-- <script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script> -->

<!--end::Page Vendors -->

<!--begin::Page Scripts -->
<!-- <script src="assets/app/js/dashboard.js" type="text/javascript"></script> -->

</body>

<!-- end::Body -->
<script src="http://formbuilder.online/assets/js/form-builder.min.js"></script>
<!--end::Page Scripts -->
</html>