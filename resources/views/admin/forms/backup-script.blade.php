<?php /* Manage Forms status script  start*/ ?>

@if($form['status']=="1")
<form method="post" action="{{ url('admin/form/update/status') }}/<?= $form['id']; ?>">
@csrf
<input type="hidden" name="status" value="0">
<button type="submit" class="btn btn-warning">Disapprove</button>
</form>

@else
<form method="post" action="{{ url('admin/form/update/status') }}/<?= $form['id']; ?>">
@csrf
<input type="hidden" name="status" value="1">
<button type="submit" class="btn btn-info">Approve</button>

</form>

@endif
<?php /* Manage Forms status script stop */ ?>




