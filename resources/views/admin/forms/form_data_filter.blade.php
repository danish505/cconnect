@extends('layouts.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Employee Data Report
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">

                    <!--begin: Search Form -->
                    <form class="m-form m-form--fit m--margin-bottom-20" method="post" action="{{ route('admin.form.data_post') }}">
                        @csrf
                        <div class="row m--margin-bottom-20">
                            <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                <label>Project:</label>
                                <select class="form-control m-select2 project" id="kt_select2_3" name="project">
                                    <option selected>Please select</option>
                                    @foreach($data['project'] as $project)
                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                <label>Channel:</label>
                                <select class="form-control kt-select2 channel" id="kt_select2_1" name="channel">
                                    <option selected value="">Please select</option>

                                </select>
                            </div>

                        </div>
                        <div class="row m--margin-bottom-20">
                        </div>
                        <div class="m-separator m-separator--md m-separator--dashed"></div>
                        <div class="row">
{{--                            <div class="col-lg-12">--}}
{{--                                <button class="btn btn-primary m-btn m-btn--icon" id="m_search">--}}
{{--                            <span>--}}
{{--                                <i class="la la-search"></i>--}}
{{--                                <span>Search</span>--}}
{{--                            </span>--}}
{{--                                </button>--}}
{{--                            </div>--}}
                        </div>
                    </form>

                    <!--begin: Datatable -->
                    <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 895px;">
                                    <thead class="tbl_head">


                                    </thead>
                                    <tbody class="tbl_body">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('form-data-script')

    <script>
        $('.project').change(function(){
            var id = $('.project').val();

            $('.channel').empty();

            $('.tbl_body').empty();
            $('.tbl_head').empty();


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    // 'Content-Type': 'application/json'
                    'Access-Control-Allow-Origin':'*'
                },
                url:"{{ url('admin/forms/project/map/callback') }}",
                method:"POST",
                data:{ id: id },


                success: function( response ){
                    // Got a response
                    cdata = JSON.parse(response);

                    channel_map = cdata.channel_map;
                    // console.log(cdata.channel_map);

                    for(i=0; i < channel_map.length; i++){
                        $('.channel').append("<option value=''>please select </option>");
                        $('.channel').append("<option value="+channel_map[i].id+">"+ channel_map[i].name+"</option>");


                    }
                    $('.channel').change('click',function(){

                        channel =  $('.channel').val();
                        console.log(channel);


                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                // 'Content-Type': 'application/json'
                                'Access-Control-Allow-Origin':'*'
                            },
                            url:"{{ url('admin/forms/project/channel/callback') }}",
                            method:"POST",
                            data:{ channel_id: channel,project_id:id },
                            success: function( channel ) {

                                form = JSON.parse(channel);

                                formFields =  JSON.parse(form.form_map[0].form_fields);





                                //  console.log(formFields.length);

                                for(j=0; j<formFields.length; j++){


                                    $('.tbl_head').append("<th>"+ formFields[j].label +"</th>");

                                    }
                                
                                for(k=0; k<form.form_data.length; k++){

                                    $('.tbl_body').append("<tr>");

                                    clean_data =  JSON.parse(form.form_data[k].data);
                                    for(l=0; l<clean_data.length; l++){

                                        $('.tbl_body').append("<td>"+ clean_data[l].userValue +"</td>");
                                    }
                                    $('.tbl_body').append("</tr>");

                                }


                            },
                            error: function( jqXHR ){

                                // Something went wrong
                                console.log( 'Error getting the channel' );
                                console.log( jqXHR );

                            }
                        });

                    })


                },
                error: function( jqXHR ){

                    // Something went wrong
                    console.log( 'Error saving the form (details below)' );
                    console.log( jqXHR );

                }
            })
        });
    </script>

@endsection