@extends('layouts.master')


@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">Dashboard</h3>
                </div>
            </div>
        </div>

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            View Forms
                        </h3>
                    </div>
                </div>
            </div>

        {{ $forms['form-fields'] }}
        <div class="row">
                    <div id="fb-render" class="col-md-12 p-5">
                    </div>
        </div>
            </div>
        </div>

    </div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-render.min.js"></script>
<script>

    jQuery(function($) {
        var fbRender = document.getElementById('fb-render'),
            formData = <?= $forms['form_fields']?>;

        var formRenderOpts = {
            formData,
            dataType: 'json'
        };

        $(fbRender).formRender(formRenderOpts);
    });
</script>

@endsection

