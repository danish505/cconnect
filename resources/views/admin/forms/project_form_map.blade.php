@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Project Form Map
                            <a href="{{ route('admin.project.form.create') }}" class="btn btn-primary float-right ">Create Project Map</a>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Project</th>
                                    <th>Channel</th>
                                    <th>Form</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($forms as $form)
                                    <?php $id ++; ?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $form->project_name }}</td>
                                        <td>{{ $form->channel_name }}</td>
                                        <td>{{ $form->form_title }}</td>
                                        <td class="action-column">
                                            <a href="{{ url('admin/forms/project/map/edit') }}/<?= $form->id ?>" class="btn btn-success">Edit </a>

                                            <a href="{{ url('admin/forms/project/map/delete') }}/<?= $form->id ?>" class="btn btn-danger">Delete </a>
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection