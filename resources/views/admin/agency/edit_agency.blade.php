@extends('layouts.master')

@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Agency
                        </h3>
                    </div>
                </div>




                <form method="post" action="{{ url('admin/agency/edit') }}/<?= $agency->id?>">
                    @csrf
                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group m-form__group pt-5">
                                <label>Agency Name</label>
                                <input type="text" class="form-control m-input" value="{{ $agency->name }}" name="agency" placeholder="Enter Agency">
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group m-form__group pt-5">
                                <label>Agency Address</label>

                                <input type="text" class="form-control m-input" value="{{ $agency->address }}" name="address" placeholder="Enter Address">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group m-form__group pt-5">
                                <label>Agency Email</label>

                                <input type="email" class="form-control m-input" name="email" value="{{ $agency->email }}" placeholder="Enter Email">
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group m-form__group pt-5">
                                <label>Agency Phone</label>

                                <input type="text" value="{{ $agency->phone }}" class="form-control m-input" name="phone" placeholder="Enter Phone">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-5">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>

                </form>


            </div>


        </div>
    </div>



@endsection