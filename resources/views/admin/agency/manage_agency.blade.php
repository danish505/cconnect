@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Agency
                            <a href="{{ route('admin.agency.create') }}" class="btn btn-primary float-right">Add Agencies</a>
                        </h3>
                        <br/>

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Agency</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($agency as $key)
                                    <?php $id++;?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $key->name }}</td>
                                        <td>
                                            <a href="{{ url('admin/agency/edit/') }}/<?= $key->id ?>" class="btn btn-success">Edit </a>
                                            <a href="#" onclick="ConfirmDelete(<?= $key->id ?>)" class="btn btn-danger">Delete </a>
                                        </td>

                                    </tr>

                                @endforeach




                                </tbody>
                            </table>
                        </div>
                        {{ $agency->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function ConfirmDelete(agencyid) {
            console.log(agencyid);

            var x = confirm("Are you sure you want to delete ?");
            if (x) {

                window.location.href = "{{ url('admin/agency/delete') }}/"+agencyid;
            } else {

                return false;
            }
        }

    </script>

@endsection