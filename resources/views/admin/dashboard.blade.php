@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="custom-date-form">
                <div class="m-portlet ">
                    <div class="m-portlet__body  m-portlet__body--no-padding">
                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-6 col-xl-3">

                                <!--begin::Total Profit-->
                                <div class="m-widget24">
                                    <div class="m-widget24__item">
                                        <h4 class="m-widget24__title">
                                            Total Projects
                                        </h4><br>
                                        <span class="m-widget24__desc">

												</span>
                                        <span class="m-widget24__stats m--font-brand">
													{{ $count['projects'] }}
												</span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            <div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="m-widget24__change">

												</span>
                                        <span class="m-widget24__number">

												</span>
                                    </div>
                                </div>

                                <!--end::Total Profit-->
                            </div>
                            <div class="col-md-12 col-lg-6 col-xl-3">

                                <!--begin::New Feedbacks-->
                                <div class="m-widget24">
                                    <div class="m-widget24__item">
                                        <h4 class="m-widget24__title">
                                           Total Channels
                                        </h4><br>
                                        <span class="m-widget24__desc">

												</span>
                                        <span class="m-widget24__stats m--font-info">
												{{ count($count['channels'])}}
												</span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            <div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="m-widget24__change">

												</span>
                                        <span class="m-widget24__number">

												</span>
                                    </div>
                                </div>

                                <!--end::New Feedbacks-->
                            </div>
                            <div class="col-md-12 col-lg-6 col-xl-3">

                                <!--begin::New Orders-->
                                <div class="m-widget24">
                                    <div class="m-widget24__item">
                                        <h4 class="m-widget24__title">
                                            Total Submitted Forms
                                        </h4><br>
                                        <span class="m-widget24__desc">

												</span>
                                        <span class="m-widget24__stats m--font-danger">
													{{ $count['forms'] }}
												</span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="m-widget24__change">

												</span>
                                        <span class="m-widget24__number">

												</span>
                                    </div>
                                </div>

                                <!--end::New Orders-->
                            </div>
                            <div class="col-md-12 col-lg-6 col-xl-3">

                                <!--begin::New Users-->
                                <div class="m-widget24">
                                    <div class="m-widget24__item">
                                        <h4 class="m-widget24__title">
                                            Total Users
                                        </h4><br>
                                        <span class="m-widget24__desc">

												</span>
                                        <span class="m-widget24__stats m--font-success">
													{{ $count['users'] }}
												</span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="m-widget24__change">

												</span>
                                        <span class="m-widget24__number">

												</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

            </div>


                <div class="kt-portlet mng-users-table">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Pending Individual Accounts
                            </h3>
                        </div>
                    </div>

                                <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12"><table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 895px;">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending">Id</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Image</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Name</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >CNIC</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Project</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Channel</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Team</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Status</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" >Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">Id</th>
                                        <th rowspan="1" colspan="1">Image</th>
                                        <th rowspan="1" colspan="1">Name</th>
                                        <th rowspan="1" colspan="1">CNIC</th>
                                        <th rowspan="1" colspan="1">Project</th>
                                        <th rowspan="1" colspan="1">Channel</th>
                                        <th rowspan="1" colspan="1">Team</th>
                                        <th rowspan="1" colspan="1">Status</th>
                                        <th rowspan="1" colspan="1">Action</th>
                                    </tr>
                                    <tbody>
                                    <?php $id = 0; ?>
                                    @foreach($users as $user)
                                        <?php $id ++;
                                            $channel_id = explode(',',$user->channel_id);

                                        ?>

                                        <tr role="row" class="odd">
                                            <th scope="row">{{ $id }}</th>
                                            <td><img src="{{ asset('uploads/employee') }}/<?= $user->image; ?>" width="50"/></td>

                                            <td><a href="{{ url('admin/user/profile') }}/{{ $user->employee_id }}">{{ $user->employee_name }}</a></td>
                                            <td>{{ $user->cnic }}</td>
                                            <td>{{ $user->project_name }}</td>
                                            <td>
                                                @foreach($count['channels'] as $chann)
                                                @if(in_array($chann->id,$channel_id))
                                                        {{ $chann->name }}
                                                 @endif
                                                @endforeach
                                            <td>{{ $user->team_name }}</td>
                                            </td>

                                            <form method="post" action="{{ url('admin/users/update/status/') }}/<?= $user->employee_id; ?>">
                                @csrf
                                <td>
                                    <select class="form-control" name="status">
                                        <option selected>Please Select</option>
                                        <option value="approved" @if($user->status=='approved') selected @endif>Approved</option>
                                        <option value="disapproved" @if($user->status=='disapproved') selected @endif>DisApproved</option>
                                    </select>
                                </td>
                                    <td><button type="submit" class="btn btn-success">Submit </button></td>
                            </form>
                                        </tr>
                                    @endforeach

                                    </tbody>
                           </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection