@extends('layouts.master')


@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           {{ $data['project'][0]->name }}

                        </h3>
                    </div>
                </div>
                    <div class="page-header">
                        <h3> Channels</h3>
                    </div>

{{--                <form method="post" action="{{ route('admin.project.form.map') }}">--}}
                    @csrf
                    @foreach($data['channels'] as $channel)
                    <div class="row">


                    <div class="col-md-3 pt-5">

                      <label>{{ $channel->name }}</label>

                    </div>

                    <div class="col-md-3 pt-5 ">


                     <a href="{{ url('admin/projects/channel/form/map/') }}/{{ $data['project'][0]->id }}/{{ $channel->id }}" class="btn btn-success">Form</a>


                    </div>
                    </div>
                    @endforeach

{{--                    <div class="row">--}}
{{--                        <div class="col-md-4">--}}
{{--                            <div class="form-group m-form__group pt-5">--}}
{{--                                <input type="submit" class="btn btn-primary">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}


            </div>
            </div>


        </div>

@endsection
