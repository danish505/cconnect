@extends('layouts.master')

@section('content')

<?php



?>


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-lg-12">

                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                    <h3 class="m-portlet__head-text">
                                        Edit Project
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <!--begin::Form-->
                         @foreach($project as $key)
                        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
                              action="{{ url('admin/projects/edit/') }}/<?= $key->id;?>">
                            @csrf

                           <div class="form-group m-form__group row">

                                    <div class="col-lg-6">
                                        <?php
                                        $start_date = strtotime($key->start_date);
                                        ?>
                                        <label class="">Start Date:</label>
                                        <input class="form-control m-input" type="date" name="start_date" min="{{ date('Y-m-d') }}" id="example-date-input" value="{{date('Y-m-d',$start_date)}}">
                                        <span class="m-form__help">Please enter Project start date</span>
                                    </div>

                                    <div class="col-lg-6">
                                        <label>Expiry Date:</label>
                                        <?php
                                        $expiry_date = strtotime($key->expiry_date);

                                        ?>
                                        <input type="date" min="{{ date('Y-m-d') }}" name="exp_date" value="{{ date('Y-m-d',$expiry_date) }}" class="form-control" placeholder="Select date">
                                        <span class="m-form__help">Please enter Project expiry date</span>
                                    </div>
                                </div>

                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions--solid">
                                    <div class="row">
                                        <div class="col-lg-6"></div>
                                        <div class="col-lg-6 float-right">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <a href="{{ route('admin.projects') }}" class="btn btn-secondary">Cancel</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



