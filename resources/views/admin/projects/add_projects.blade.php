@extends('layouts.master')


@section('content')

    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn"><i class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown " data-menu-vertical="true" m-menu-dropdown="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="index.html" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Dashboard</span></a></li>
                    <li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="index.html" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon fa fa-users"></i><span class="m-menu__link-text">Manage Users</span></a></li>
                    <li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="index.html" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-list-1"></i><span class="m-menu__link-text">Manage Users</span></a></li>

                </ul>
            </div>

            <!-- END: Aside Menu -->
        </div>

        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">Create Project</h3>
                        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                            <li class="m-nav__item m-nav__item--home">
                                <a href="#" class="m-nav__link m-nav__link--icon">
                                    <i class="m-nav__link-icon la la-home"></i>
                                </a>
                            </li>
                            <li class="m-nav__separator">-</li>
                            <li class="m-nav__item">
                                <a href="" class="m-nav__link">
                                    <span class="m-nav__link-text">Forms &amp; Controls</span>
                                </a>
                            </li>
                            <li class="m-nav__separator">-</li>
                            <li class="m-nav__item">
                                <a href="" class="m-nav__link">
                                    <span class="m-nav__link-text">Form Layouts</span>
                                </a>
                            </li>
                            <li class="m-nav__separator">-</li>
                            <li class="m-nav__item">
                                <a href="" class="m-nav__link">
                                    <span class="m-nav__link-text">Multi Column Forms</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div>
                    </div>
                </div>
                <div></div></div>
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="row">
                    <div class="col-lg-12">

                        <!--begin::Portlet-->
                        <div class="m-portlet">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                        <h3 class="m-portlet__head-text">
                                            Create Project
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            <!--begin::Form-->
                            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-6">
                                            <label>Brand:</label>
                                            <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup"><select class="form-control m-bootstrap-select m_selectpicker" multiple="" tabindex="-98">
                                                    <option>Mustard</option>
                                                    <option>Ketchup</option>
                                                    <option>Relish</option>
                                                </select>
                                            </div>
                                            <span class="m-form__help">Please enter your contact number</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Channel:</label>
                                            <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup"><select class="form-control m-bootstrap-select m_selectpicker" multiple="" tabindex="-98">
                                                    <option>Mustard</option>
                                                    <option>Ketchup</option>
                                                    <option>Relish</option>
                                                </select>
                                            </div>
                                            <span class="m-form__help">Please enter your contact number</span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-6">
                                            <label>Cities:</label>
                                            <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup"><select class="form-control m-bootstrap-select m_selectpicker" multiple="" tabindex="-98">
                                                    <option>Karachi</option>
                                                    <option>Lahore</option>
                                                    <option>Islambad</option>
                                                </select>
                                            </div>
                                            <span class="m-form__help">Please enter your contact number</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="">Team:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input type="text" class="form-control m-input" placeholder="Enter your postcode">
                                            </div>
                                            <span class="m-form__help">Please enter your postcode</span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-6">
                                            <label class="">Project Name:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input type="text" class="form-control m-input" placeholder="Enter your postcode">
                                            </div>
                                            <span class="m-form__help">Please enter your postcode</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="">Start Date:</label>
                                            <div class="m-input-icon m-input-icon--right">
                                                <input type="date" name="date_from" class="form-control" placeholder="Select date">
                                            </div>
                                            <span class="m-form__help">Please enter your postcode</span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-lg-6">
                                            <label>Status:</label>
                                            <div class="m-radio-inline">
                                                <label class="m-radio m-radio--solid">
                                                    <input type="radio" name="example_2" checked="" value="2"> Active
                                                    <span></span>
                                                </label>
                                                <label class="m-radio m-radio--solid">
                                                    <input type="radio" name="example_2" value="2"> Inactive
                                                    <span></span>
                                                </label>
                                            </div>
                                            <span class="m-form__help">Please select user Status</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Expiry Date:</label>
                                            <input type="date" name="date_from" class="form-control" placeholder="Select date">
                                            <span class="m-form__help">Please enter your contact number</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions--solid">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <button type="reset" class="btn btn-primary">Save</button>
                                                <button type="reset" class="btn btn-secondary">Cancel</button>
                                            </div>
                                            <div class="col-lg-6 m--align-right">
                                                <button type="reset" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    @endsection