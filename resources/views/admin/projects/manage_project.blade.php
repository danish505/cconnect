@extends('layouts.master')


@section('content')



    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage projects
                            <a href="{{ route('admin.projects.create') }}" class="btn btn-primary float-right">Add projects</a>
                        </h3>
                        <br/>

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>project</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($projects as $project)
                                    <?php $id++;?>
                            <tr>
                                <th scope="row">{{ $id }}</th>
                                <td>{{ $project->name }}</td>
                                <td>
                                <a href="{{ url('admin/projects/form/map/edit/') }}/<?= $project->id; ?>" class="btn btn-info">Map Form </a>
                                <a href="{{ url('admin/projects/edit/') }}/<?= $project->id; ?>" class="btn btn-success">Edit Expiry</a>
                                    <input type="button" onclick="ConfirmDelete(<?= $project->id; ?>)" value="In Active" class="btn btn-warning">
                                </td>

                            </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{ $projects->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        function ConfirmDelete(projectid) {

            var x = confirm("Are you sure you want to delete ?");
            if (x) {
                window.location.href = "{{ url('admin/projects/delete') }}/"+projectid;

            } else {

                return false;
            }
        }


    </script>



@endsection

