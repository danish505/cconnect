@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                                <span class="m-portlet__head-icon m--hide">
                                                    <i class="la la-gear"></i>
                                                </span>
                                    <h3 class="m-portlet__head-text">
                                        Create Project
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" action="{{ route('admin.projects.store') }}">
                            @csrf
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label class="">Project Name:</label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" name="name"  placeholder="Enter Project Name" required>
                                    </div>
                                    <span class="m-form__help">Please enter your Project Name</span>
                                </div>
                                <div class="col-lg-6">
                                    <label>Agency:</label>
                                    <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="agency[]" tabindex="-98" required>

                                        @foreach($data['agency'] as $agency)
                                                <option value="{{$agency['id']}}">{{$agency['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <span class="m-form__help">Please select your Agency</span>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label>Brand:</label>
                                        <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup">
                                <select class="form-control m-bootstrap-select m_selectpicker" multiple name="brand[]" tabindex="-98" required>
                                                @foreach($data['brands'] as $brand)
                                                    <option value="{{$brand['id']}}">{{$brand['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="m-form__help">Please select your Brands</span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Channel:</label>
                                        <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup">
                                            <select class="form-control m-bootstrap-select m_selectpicker" name="channel[]" multiple  tabindex="-98" required>
                                                @foreach($data['channels'] as $channel)
                                                    <option value="{{ $channel['id'] }}" >{{ $channel['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="m-form__help">Please select your Channels</span>
                                    </div>
                                </div>
                                <div class='row m--margin-bottom-10-tablet-and-mobile p-3'>
                                    <div class='col-lg-6'>
                                        <label>City:</label>
                                        <select class='form-control kt-select2 width-100'id='m_select2_1' name='city[]'>
                                            @foreach($data['cities'] as $city)
                                                <option value="{{ $city['id'] }}" >{{ $city['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class='col-lg-6'>
                                        <label>Team:</label>
                                        <select class='form-control kt-select2 width-100' multiple id='kt_select2_2' name='team[0][]'>
                                            @foreach($data['teams'] as $team)
                                                <option value="{{ $team['id'] }}" >{{ $team['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="demo4">
                                    <!-- <div class="col-lg-12 m--margin-bottom-10-tablet-and-mobile">
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">

                                    </div>
                                    <div class="col-lg-6">
                                        <button id="remove-btn" class="btn btn-primary" type="button">Remove</button>
                                       <button id="like" type="button" class="btn-primary btn add-more-btn" >Add More</button>
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label class="">Start Date:</label>
                                        <input class="form-control m-input" type="date" name="start_date" min="{{ date('Y-m-d') }}" id="example-date-input" required>
                                        <span class="m-form__help">Please enter Project start date</span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Expiry Date:</label>
                              <input type="date" min="{{ date('Y-m-d') }}" name="exp_date"  class="form-control" placeholder="Select date" required>
                                        <span class="m-form__help">Please enter Project expiry date</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>Status:</label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" checked="" value="1"> Active
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid">
                                            <input type="radio" name="status" value="0"> Inactive
                                            <span></span>
                                        </label>
                                    </div>
                                    <span class="m-form__help">Please select Project status</span>
                                </div>
                                <div class="col-lg-6"></div>

                            </div>
                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions--solid">
                                    <div class="row">
                                        <div class="col-lg-6"></div>
                                        <div class="col-lg-6 float-right">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('create-project-script')

    <script src="{{ asset('assets/demo/demo3/base/select2a.js') }}" type="text/javascript"></script>


    <script>
        i = 3;
        $("#remove-btn").hide();
        j=0;
        $('#like').on("click",function(){
            i++;
            j++;
            $('#demo4').each(function(){


        $("#demo4").append("<div id='remove_div_"+j+"' class='row m--margin-bottom-10-tablet-and-mobile p-3'><div class='col-lg-6'><label>City:</label><select class='form-control kt-select2 width-100'"+ "id="+'m_select2_'+j+ " name='city[]' required><?php foreach($data['cities'] as $city):
          echo  "<option value='".$city['id']."'>".$city['name']."</option>";
            endforeach; ?> </select></div><div class='col-lg-6'><label>Team:</label><select class='form-control kt-select2 width-100'"+ "id="+'kt_select2_'+i+ "  multiple name='team["+j+"][]' required><?php foreach($data['teams'] as $team): echo"<option value='".$team['id']."'>".$team['name']."</option>"; endforeach; ?></select></div></div>");

        });
            if (j>0)
            {
                $("#remove-btn").show();

                $("#remove-btn").on('click',function(){
                    $("#demo4").each(function(){
                        $("#remove_div_"+j).remove();
                j--;
                    });

                    console.log(j);

                });
            }
        });

    </script>
@endsection