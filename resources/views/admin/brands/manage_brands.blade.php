@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Brands
                            <a href="{{ route('admin.brands.create') }}" class="btn btn-primary float-right">Add Brands</a>
                        </h3>
                        <br/>

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Brand</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($brands as $brand)
                                    <?php $id++;?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $brand->name }}</td>
                                        <td>
                                            <a href="{{ url('admin/brands/edit/') }}/<?= $brand->id ?>" class="btn btn-success">Edit </a>
                                            <a href="#" onclick="ConfirmDelete(<?= $brand->id ?>)"  class="btn btn-danger">Delete </a>
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{ $brands->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function ConfirmDelete(brandid) {

            var x = confirm("Are you sure you want to delete ?");
            if (x) {
                window.location.href = "{{ url('admin/channels/delete') }}/"+brandid;

            } else {

                return false;
            }
        }

    </script>

@endsection