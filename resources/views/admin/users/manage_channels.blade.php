@extends('layouts.master')

@section('content')
<?php
//$data_array = null;

//print_r($data_array);
//die();
?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage User Channels
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">

        <form class="m-form m-form--fit m--margin-bottom-20" method="post" action="{{ route('admin.user.channel.search') }}">
                        @csrf
                <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>Name:</label>
                        <input type="text" class="form-control m-input" name="name" placeholder="Name" data-col-index="0">
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>Project:</label>
                        <input type="text" class="form-control m-input" name="project" placeholder="Project" data-col-index="1">
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>Channel:</label>
                        <input type="text" class="form-control m-input" name="channel" placeholder="Channel" data-col-index="1">
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>Date:</label>
                    <div class="input-daterange input-group" id="m_datepicker">
                    <input class="form-control m-input" type="date" name="start_date" min="{{ date('Y-m-d') }}" id="example-date-input">
                    <div class="input-group-append">
                    <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                    </div>
                    <input class="form-control m-input" type="date" name="expiry_date" min="{{ date('Y-m-d') }}" id="example-date-input">
                    </div>
                    </div>

                </div>
                    <div class="row m--margin-bottom-20">

                    </div>
                    <div class="m-separator m-separator--md m-separator--dashed"></div>
                <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary m-btn m-btn--icon" id="m_search">
                                    <span>
                                        <i class="la la-search"></i>
                                        <span>Search</span>
                                    </span>
                    </button>
                    &nbsp;&nbsp;
                    <button class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
                                    <span>
                                        <i class="la la-close"></i>
                                        <span>Reset</span>
                                    </span>
                    </button>
                </div>
            </div>
            </form>
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>CNIC</th>
                                    <th>Projects</th>
                                    <th>Channel</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($users as $user)
                                    <?php $id ++;  ?>

                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $user->employee_name }}</td>
                                        <td>{{ $user->cnic }}</td>
                                        <td>{{ $user->project_name }}</td>
                                        <td style="font-size:14px;">
                                @foreach($channels as $channel)

                                   <?php $data = explode(',',$user->channel_id);?>
                                    @if(in_array($channel['id'],$data))
                                        <p>{{ $channel['name']}}</p>
                                    @endif
                                 @endforeach
                                        </td>
                                        <td><a href="{{ url('admin/users/channels/edit') }}/<?= $user->id ?>" class="btn btn-primary">Edit</a>
{{--                                            <a href="#" class="btn btn-danger">Delete</a></td>--}}

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection