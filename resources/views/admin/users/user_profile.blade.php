@extends('layouts.master')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">My Profile</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="{{ asset('uploads/employee') }}/{{ $employee->image }}" alt="">
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">{{ $employee->name }}</span>
                                <a href="" class="m-card-profile__email m-link">{{ $employee->email }}</a>
                            </div>
                        </div>
                        <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="m-nav__section m--hide">
                                <span class="m-nav__section-text">Section</span>
                            </li>
                            <li class="m-nav__item">
                                <a  class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-title">
														<span class="m-nav__link-wrap">
															<span class="m-nav__link-text">My Profile</span>
														</span>
													</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a   class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-share"></i>
                                    <span class="m-nav__link-text">{{ $employee->email }}</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a  class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                                    <span class="m-nav__link-text">{{ $employee->agency_name }}</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a  class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                    <span class="m-nav__link-text">{{ $employee->project_name }}</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a  class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-time-3"></i>
                                    <span class="m-nav__link-text">{{ $employee->channel_name }}</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                    <span class="m-nav__link-text">{{ $employee->team_name }}</span>
                                </a>
                            </li>
                        </ul>
                        <div class="m-portlet__body-separator"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head display-none">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Update Profile
                                    </a>
                                </li>

                            </ul>
                        </div>

                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">

                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group m--margin-top-10 m--hide">
                                        <div class="alert m-alert m-alert--default" role="alert">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">1. Personal Details</h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Name</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->name }}" type="text" placeholder="Mark Andre">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Father Name</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="{{ $employee->fathername }}" placeholder="Father Name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">CNIC</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->cnic }}" type="text" placeholder="CNIC">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Email</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->email }}" type="Email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">City</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->city_name }}" type="text" placeholder="City">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Phone Number</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->cell }}" type="text" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Agency Email</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->agency_email }}" type="Email" placeholder="Agency Email">
                                        </div>
                                    </div>

                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Registered Date</label>
                                        <div class="col-7">

                                            <input class="form-control m-input" type="date" value="<?= date('m-d-Y',strtotime($employee->created_at)); ?>" id="example-date-input">
                                        </div>
                                    </div>
                                    <div class="seperator-div"></div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">2. Project Details</h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Agency Name</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="{{ $employee->agency_name }}" placeholder="Agency Name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Project Name</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="{{ $employee->project_name }}" placeholder="Project Name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">City</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="{{ $employee->city_name }}" placeholder="City">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Channel</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->channel_name }}" type="text" placeholder="Channel">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Team</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" value="{{ $employee->team_name }}" type="text" placeholder="Team">
                                        </div>
                                    </div>
                                    <div class="seperator-div"></div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">3. Location</h3>
                                        </div>
                                    </div>

                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Google Map</label>
                                        @if(isset($last_login))
                           <a href="https://www.google.com/maps/{{$last_login->lat}},{{$last_login->log}}" class="map-link">Location
                                            </a>
                                        @endif
                                    </div>

                                </div>

            <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions my-profile-actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-7">
                                <div class="row">
                                    <div class="col-md-4 float-right">
                                    <form method="post" action="{{ url('admin/users/update/status') }}/<?= $employee->emp_id; ?>">
                                        @csrf
                                <input type="hidden" name="status" value="approved">
                                <button type="submit" class="btn m-btn btn-primary m-btn--air m-btn--custom">
                                    <i class="fa fa-check"></i>
                                    Approve
                                </button>
                                </form>
                                    </div>
                                    <div class="col-md-4 ">
                                <form method="post" action="{{ url('admin/users/update/status/') }}/<?= $employee->emp_id; ?>">
                                 @csrf
                                    <input type="hidden" name="status" value="disapproved">
                                    <button type="reset" class="btn btn-danger btn-accent m-btn m-btn--air m-btn--custom">
                                    <span style="font-weight: 800;">X</span>
                                    Disapprove
                                </button>
                                </form>
                                    </div>
                                        <div class="col-md-4">
                                    <a href="{{ url('admin/') }}" class="btn btn-accent m-btn m-btn--air m-btn--custom">Cancel</a>
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
            </div>
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_2">
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
