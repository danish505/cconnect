@extends('layouts.master')

@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            New Project Requests
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Channel</th>
                                    <th>Project</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($users as $user)
                                    <?php $id ++;  ?>

                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $user->employee_name }}</td>
                                        <td>{{ $user->channel_name }}</td>
                                        <td>{{ $user->project_name }}</td>

                                        <form method="post" action="{{ url('admin/projects/status/update/') }}/<?= $user->employee_id; ?>">

                                            @csrf
                                            <td>
                                                <select class="form-control" name="status">
                                                    <option value="1" @if($user->status=='1') selected @endif>Approved</option>
                                                    <option value="0" @if($user->status=='0') selected @endif>DisApproved</option>
                                                </select>

                                            </td>
                                            <td><button type="submit" class="btn btn-success">Submit </button></td>
                                        </form>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection