@extends('layouts.master')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Online Users Search
                    </h3>
                </div>
            </div>
        </div>






        <div class="m-portlet__body">

            <!--begin: Search Form -->
            <form class="m-form m-form--fit m--margin-bottom-20" method="get" action="{{ route('admin.users.online.search') }}">
               @csrf
                <div class="row m--margin-bottom-20">

                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                        <label>User Name </label>
                        <select class="form-control m-select2" id="kt_select2_2" name="employee">
                            <option selected value="">Please Select </option>
                            @foreach($data['employees'] as $employee)
                                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->any()) <span class="text-danger">{{ $errors->first('employee') }} </span> @endif
                    </div>

                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                        <label>Project </label>
                        <select class="form-control m-select2" id="kt_select2_3" name="project" >
                            <option selected value="">Please Select</option>
                            @foreach($data['project'] as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->any()) <span class="text-danger">{{ $errors->first('project') }} </span> @endif
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                        <label>Channel </label>
                        <select class="form-control m-select2" id="kt_select2_4" name="channel" >
                            <option selected value="">Please Select</option>
                            @foreach($data['channels'] as $channel)
                                <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->any()) <span class="text-danger">{{ $errors->first('channel') }} </span> @endif
                    </div>
                </div>
                <div class="row m--margin-bottom-20">
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                        <label>City </label>
                        <select class="form-control m-select2" id="kt_select2_5" name="city" >
                            <option selected value="">Please Select</option>
                            @foreach($data['city'] as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->any()) <span class="text-danger">{{ $errors->first('city') }} </span> @endif
                    </div>
                </div>
                <div class="row m--margin-bottom-20">
                </div>
                <div class="m-separator m-separator--md m-separator--dashed"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary m-btn m-btn--icon" id="m_search">
                            <span>
                                <i class="la la-search"></i>
                                <span>Search</span>
                            </span>
                        </button>
                    </div>
                </div>
            </form>


            <!--begin: Datatable -->
            <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12"><table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 895px;">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="	" aria-label="Record ID: activate to sort column descending" aria-sort="ascending">
                                    S.NO
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Order ID: activate to sort column ascending">
                                    Image
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Order ID: activate to sort column ascending">
                                    Activation Partner
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Order ID: activate to sort column ascending">
                                    User Name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Country: activate to sort column ascending">
                                    Project
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Ship City: activate to sort column ascending">
                                    City
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Company Agent: activate to sort column ascending">
                                    Channel
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Ship Date: activate to sort column ascending">
                                    Clock In
                                </th>
                                 
                                <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="" aria-label="Status: activate to sort column ascending">
                                Clockin Location</th>
                               
                                 
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">S.NO</th>
                                <th rowspan="1" colspan="1">Clockin Image</th>
                                <th rowspan="1" colspan="1">Activation Partner</th>
                                <th rowspan="1" colspan="1">User Name</th>
                                <th rowspan="1" colspan="1">Project</th>
                                <th rowspan="1" colspan="1">City</th>
                                <th rowspan="1" colspan="1">Channel</th>
                                <th rowspan="1" colspan="1">Clock In</th>
                                <th rowspan="1" colspan="1">Clockin Location</th>
                                
                                
                            </tr>
                            </tfoot>
                            <tbody><?php $id = 0; $i=0;?>
                            @foreach($employees as $key)
                                <?php $id++ ?>
                                <tr role="row" class="odd">
                                <td class="sorting_1" tabindex="0">{{ $id }}</td>
                                <td><img src="{{ asset('uploads/clock_in').'/'.$key->clockin_image }}" width="50px"/></td>
                                <td>{{ $key->agency_name }}</td>
                                <td>{{ $key->employee_name }}</td>
                                <td>{{ $key->project_name }}</td>
                                <td>{{ $key->city_name }}</td>

                                <td> <?php $channel = explode(',',$key->channel_id); ?>
                                    @foreach($data['channels'] as $val)
                                        @if(in_array($val->id,$channel))
                                            {{ $val->name }}
                                        @endif
                                    @endforeach
                                </td>
                             
                                <td>{{ $key->clock_in}}</td>
                                    
                                        
                                        <td> <a href="http://maps.google.com/maps?q=<?= $key->clockin_lat.','. $key->clockin_log;?>" target="_blank">Location</a></td>

                                        
                                     
                             </tr>
                           
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="m_table_1_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries (filtered from 350 total entries)</div></div><div class="col-sm-12 col-md-7 dataTables_pager"><div class="dataTables_length" id="m_table_1_length"><label>Display <select name="m_table_1_length" aria-controls="m_table_1" class="custom-select custom-select-sm form-control form-control-sm"><option value="5">5</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select></label></div><div class="dataTables_paginate paging_simple_numbers" id="m_table_1_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="m_table_1_previous"><a href="#" aria-controls="m_table_1" data-dt-idx="0" tabindex="0" class="page-link"><i class="la la-angle-left"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="m_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="m_table_1_next"><a href="#" aria-controls="m_table_1" data-dt-idx="2" tabindex="0" class="page-link"><i class="la la-angle-right"></i></a></li></ul></div></div></div></div>
        </div>
    </div>
</div>
</div>
    @endsection