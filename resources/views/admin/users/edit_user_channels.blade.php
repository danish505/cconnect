@extends('layouts.master')


@section('content')
<?php
if(isset($employees)):
$data = explode(',',$employees[0]['channel_id']);

?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">Dashboard</h3>
                </div>

            </div>
        </div>

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Channels
                        </h3>
                    </div>
                </div>


                <form method="post" action="{{ url('admin/users/channels/update') }}/<?= $employees[0]['employee_id'];?> ">
                    @csrf
                    <div class="row">


{{--{{dd($emp)}}--}}
                <div class="col-md-4">

                    <div class="form-group m-form__group pt-5">
                        <label for="exampleSelectd"> Name</label>

                        <input type="text" disabled class="form-control m-input" name="name" value="{{ $employees[0]['employee_name']}}" placeholder="Enter Channel Name">

                    </div>
                </div>


                        <div class="col-lg-6">
                            <div class="form-group m-form__group pt-5">
                            <label>Channels</label>

                            <div class="dropdown bootstrap-select show-tick form-control m-bootstrap-select m_ dropup">
                            <select class="form-control m-bootstrap-select m_selectpicker" multiple="" name="channels[]" tabindex="-98">
                                @foreach($project_channel as $channel)

               <option value="{{ $channel->id}}" @if(in_array($channel->id,$data)) selected @endif>{{ $channel->name}}</option>
                                @endforeach
                         </select>
                            </div>
                        </div>
                        </div>



                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-5">
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>
<?php  else: echo "<div class='row'><div class='m-content'>user data not found</div></div>";     endif; ?>
@endsection