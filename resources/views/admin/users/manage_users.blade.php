@extends('layouts.master')

@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Users Permission
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <form class="m-form m-form--fit m--margin-bottom-20" method="post" action="{{ route('admin.user.search') }}">
                        @csrf
                        <div class="row m--margin-bottom-20">
                          <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                               <label>Activation Partner:</label>
                            <select class="form-control m-select2" id="kt_select2_1" name="agency">
                            <option >Please select</option>
                            @foreach($data['agencies'] as $agency)
                                <option value="{{ $agency->id }}">{{ $agency->name }}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                             <label>Project:</label>
                            <select class="form-control m-select2" id="kt_select2_2" name="project">
                                 <option >Please select</option>
                            @foreach($data['projects'] as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                            @endforeach
                        </select>
                        </div>
                            <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>CNIC:</label>
                               <select class="form-control m-select2" id="kt_select2_3" name="cnic">
                                    <option >Please select</option>
                            @foreach($data['employees'] as $emp)
                                <option value="{{ $emp->id }}">{{ $emp->cnic }}</option>
                            @endforeach
                        </select>
                            </div>
                            <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>Channel:</label>
                               <select class="form-control m-select2" id="kt_select2_4" name="channel">
                                    <option >Please select</option>
                            @foreach($data['channels'] as $channel)
                                <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                            @endforeach
                        </select>
                            </div>
 

                        </div>
                        <div class="row m--margin-bottom-20">

                        </div>
                        <div class="m-separator m-separator--md m-separator--dashed"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-primary m-btn m-btn--icon" id="m_search">
                                    <span>
                                        <i class="la la-search"></i>
                                        <span>Search</span>
                                    </span>
                                </button>
                                <button class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
                                    <span>
                                        <i class="la la-close"></i>
                                        <span>Reset</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>CNIC</th>
                                    <th>Agency</th>
                                    <th>Project</th>
                                    <th>Channel</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($users as $user)
                                    <?php $id ++;  ?>

                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td><img src="{{ asset('uploads/employee') }}/<?= $user->image; ?>" width="50"/></td>
                                        <td>{{ $user->employee_name }}</td>
                                        <td>{{ $user->cnic }}</td>
                                        <td>{{ $user->agency_name }}</td>
                                        <td>{{ $user->project_name }}</td>
                                        <td>{{ $user->channel_name }}</td>

                                        <form method="post" action="{{ url('admin/users/update/status/') }}/<?= $user->id; ?>">

                                            @csrf
                                            <td>
                                                <select class="form-control" name="status">
                                                    <option value="pending" @if($user->status=='pending') selected @endif>Pending</option>
                                                    <option value="approved" @if($user->status=='approved') selected @endif>Approved</option>
                                                    <option value="disapproved" @if($user->status=='disapproved') selected @endif>DisApproved</option>
                                                </select>

                                            </td>
                                            <td><button type="submit" class="btn btn-success">Submit </button></td>
                                        </form>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection