@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Teams
                            <a href="{{ route('admin.team.create') }}" class="btn btn-primary float-right">Add Team</a>
                        </h3>
                        <br/>

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Team</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($teams as $team)
                                    <?php $id++;?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $team->name }}</td>
                                        <td>
                                            <a href="{{ url('admin/teams/edit/') }}/<?= $team->id ?>" class="btn btn-success">Edit </a>
                                            <a href="#" class="btn btn-danger" onclick="ConfirmDelete(<?= $team->id ?>)" >Delete </a>
                                        </td>

                                    </tr>

                                @endforeach

                                 </tbody>
                            </table>
                        </div>
                        {{ $teams->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function ConfirmDelete(teamid) {
            console.log(teamid);

            var x = confirm("Are you sure you want to delete ?");
            if (x) {
                window.location.href = "{{ url('admin/teams/delete') }}/"+teamid;

            } else {

                return false;
            }
        }

    </script>

@endsection