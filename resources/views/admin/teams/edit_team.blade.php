@extends('layouts.master')

@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Teams
                        </h3>
                    </div>
                </div>




                <form method="post" action="{{ url('admin/teams/update') }}/<?=$team->id;?>">
                    @csrf
                    <div class="row">

                        <div class="col-md-4">

                            <div class="form-group m-form__group pt-5">
                                <input type="text" class="form-control m-input" name="name" value="{{ $team->name }}" placeholder="Enter Teams Name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-5">
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>


            </div>


        </div>
    </div>



@endsection