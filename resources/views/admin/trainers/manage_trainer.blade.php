@extends('layouts.master')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
         <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Trainer
                            <a href="{{ route('admin.trainer.create') }}" class="btn btn-primary float-right">Add Trainer</a>
                        </h3>
                        <br/>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Cnic</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($trainer as $key)
                                    <?php $id++;?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $key->name }}</td>
                                        <td>{{ $key->cnic }}</td>
                                        <td>{{ $key->email }}</td>
                                        <td>

                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{ $trainer->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function ConfirmDelete(teamid) {
            console.log(teamid);

            var x = confirm("Are you sure you want to delete ?");
            if (x) {
                window.location.href = "{{ url('admin/trainer/delete') }}/"+teamid;

            } else {

                return false;
            }
        }

    </script>

 @endsection