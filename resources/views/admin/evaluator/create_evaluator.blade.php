@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-lg-12">

                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                    <h3 class="m-portlet__head-text">
                                        Create Evaluator
                                    </h3>
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                    <!--begin::Form-->
                        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" action="{{ route('admin.evaluator.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label class="">Evaluator Name:</label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" name="name"  placeholder="Evaluator Name" required>
                                    </div>
                                    {{--                                    <span class="m-form__help">Please enter your Trainer Name</span>--}}
                                </div>
                                <div class="col-lg-6">
                                    <label>Father Name:</label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" name="fname"  placeholder="Father Name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>CNIC:</label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="number" class="form-control m-input" name="cnic"  placeholder="CNIC" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">

                                </div>

                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>Password:</label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="password" class="form-control m-input" name="password"  placeholder="Password" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label>Confirm Password:</label>
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="password" class="form-control m-input" name="password_confirmation"  placeholder="Confirm Password" required>
                                    </div>
                                </div>

                            </div>
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label>Email:</label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="email" class="form-control m-input" name="email"  placeholder="Email" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Phone:</label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="number" class="form-control m-input" name="phone"  placeholder="Phone" required>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label>Profile Image:</label>
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="file" class="form-control m-input" name="photo"  placeholder="Profile Image" required>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions--solid">
                                    <div class="row">
                                        <div class="col-lg-6"></div>
                                        <div class="col-lg-6 float-right">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection