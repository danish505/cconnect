@extends('layouts.master')


@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Channels
                        </h3>
                    </div>
                </div>

                <form method="post" action="{{ url('admin/channels/update') }}/<?= $channel[0]->channel_id;?>">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-2">
                                <label for="exampleSelectd">Channel Name</label>
                  <input type="text" class="form-control m-input" name="name" value="{{ $channel[0]->name }}" placeholder="Enter Channel Name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-2">
                                <label for="exampleSelectd">Form Title</label>
                            <input type="text" disabled class="form-control" value="{{ $channel[0]->title }}"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-5">
                                <a href="{{ url('admin/channels/form/edit/')  }}/<?= $channel[0]->form_id ?>" class="btn btn-info">View</a>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group pt-5">
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Content -->
    </div>
@endsection