@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->

        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Channels
                            <a href="{{ route('admin.channels.create') }}" class="btn btn-primary float-right">Add Channels</a>
                        </h3>
                        <br/>

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Channels</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id = 0; ?>
                                @foreach($channels as $channel)
                                    <?php $id++;?>
                                    <tr>
                                        <th scope="row">{{ $id }}</th>
                                        <td>{{ $channel->name }}</td>
                                        <td>
                            <a href="{{ url('admin/channels/edit/') }}/<?= $channel->id ?>" class="btn btn-success">Edit </a>
                          <input type="button" onclick="ConfirmDelete(<?= $channel->id; ?>)" value="Delete" class="btn btn-danger">
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{ $channels->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    function ConfirmDelete(channelid) {

        var x = confirm("Are you sure you want to delete ?");
        if (x) {
            window.location.href = "{{ url('admin/channels/delete') }}/"+channelid;

        } else {

            return false;
        }
    }

</script>
@endsection