@extends('layouts.master')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->


        <!-- END: Subheader -->
        <div class="m-content">
            <div class="kt-portlet mng-users-table">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Manage Cities
                            <a href="{{ route('admin.cities.create') }}" class="btn btn-primary float-right">Add City</a>
                        </h3>
                        <br/>

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cities</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ids = 0; ?>
                                @foreach($cities as $city)
                                    <?php $ids++;?>
                                    <tr>
                                        <th scope="row">{{ $ids }}</th>
                                        <td>{{ $city->name }}</td>
                                        <td>
                                            <a href="{{ url('admin/cities/edit/') }}/<?= $city->id ;?>" class="btn btn-success">Edit </a>
                                            <a href="#" onclick="ConfirmDelete(<?= $city->id ?>)" class="btn btn-danger">Delete </a>

                                        </td>

                                    </tr>

                                @endforeach




                                </tbody>
                            </table>
                        </div>
                        {{ $cities->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function ConfirmDelete(cityid) {

            var x = confirm("Are you sure you want to delete ?");
            if (x) {
                window.location.href = "{{ url('admin/cities/delete') }}/"+cityid;

            } else {

                return false;
            }
        }


    </script>
@endsection