<?php

use Faker\Generator as Faker;
use App\Agency;

$factory->define(Agency::class,function(Faker $faker){

    return [

//        'name'=>$faker->unique()->numberBetween($min = 1, $max = 31),
//        'channel_id'=>$faker->unique()->numberBetween($min = 1, $max = 10),
//        'cities_id'=>$faker->unique()->numberBetween($min = 1, $max = 50),
//        'team_id'=>$faker->unique()->numberBetween($min = 1, $max = 50),
        'name'=>$faker->company,
        'address'=>$faker->address,
        'email'=>$faker->email,
        'phone'=>$faker->phoneNumber,




    ];
});
