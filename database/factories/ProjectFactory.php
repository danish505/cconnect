<?php

use Faker\Generator as Faker;
use App\Projects;

$factory->define(Projects::class,function(Faker $faker){

    return [

        'brand_id'=>$faker->unique()->numberBetween($min = 1, $max = 31),
        'channel_id'=>$faker->unique()->numberBetween($min = 1, $max = 10),
        'cities_id'=>$faker->unique()->numberBetween($min = 1, $max = 50),
        'team_id'=>$faker->unique()->numberBetween($min = 1, $max = 50),
        'name'=>$faker->name,
        'start_date'=>$faker->date('Y-m-01'),
        'expiry_date'=>$faker->date('Y-m-30'),
        'status'=>'active',
        'created_ip'=>$faker->ipv4,



    ];
});
