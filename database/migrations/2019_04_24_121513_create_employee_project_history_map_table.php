<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeProjectHistoryMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_project_history_map', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('project_id');
            $table->integer('channel_id');
            $table->integer('city_id');
            $table->integer('brand_id');
            $table->integer('team_id');
            $table->ipAddress('created_ip')->nullable();
            $table->ipAddress('updated_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_project_history_map');
    }
}
