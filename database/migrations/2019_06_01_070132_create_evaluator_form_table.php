<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluatorFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluator_form', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('form_fields');

            $table->boolean('is_public')->nullable();

            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluator_form');
    }
}
